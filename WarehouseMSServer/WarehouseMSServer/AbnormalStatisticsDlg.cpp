// AbnormalStatisticsDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "WarehouseMSServer.h"
#include "AbnormalStatisticsDlg.h"
#include "afxdialogex.h"


// CAbnormalStatisticsDlg 对话框

IMPLEMENT_DYNAMIC(CAbnormalStatisticsDlg, CDialogEx)

CAbnormalStatisticsDlg::CAbnormalStatisticsDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAbnormalStatisticsDlg::IDD, pParent)
{

}

CAbnormalStatisticsDlg::~CAbnormalStatisticsDlg()
{
}

void CAbnormalStatisticsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CTRL, m_listCtrl);
}


BEGIN_MESSAGE_MAP(CAbnormalStatisticsDlg, CDialogEx)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CAbnormalStatisticsDlg 消息处理程序


BOOL CAbnormalStatisticsDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	//保留未自适应分辨率时的应用大小
	CRect rect;
	GetClientRect(&rect);
	m_potOld.x = rect.right - rect.left;
	m_potOld.y = rect.bottom - rect.top;

	//m_pWnd = GetDlgItem(IDC_STATIC_DRAW);

	////获取绘图区大小
	//GetDlgItem(IDC_STATIC_DRAW)->GetWindowRect(&m_cRectDraw);
	//ScreenToClient(&m_cRectDraw);

	
	//初始化列表
	m_listCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_listCtrl.InsertColumn(0, _T("序号"), LVCFMT_LEFT, 50);
	m_listCtrl.InsertColumn(1, _T("异常类型"), LVCFMT_LEFT, 150);
	m_listCtrl.InsertColumn(2, _T("异常编号"), LVCFMT_LEFT, 150);
	m_listCtrl.InsertColumn(3, _T("异常名称"), LVCFMT_LEFT, 150);
	m_listCtrl.InsertColumn(4, _T("故障时间"), LVCFMT_LEFT, 80);
	m_listCtrl.InsertColumn(5, _T("恢复时间"), LVCFMT_LEFT, 80);
	m_listCtrl.InsertColumn(6, _T("故障状态"), LVCFMT_LEFT, 100);
	m_listCtrl.InsertColumn(7, _T("异常说明"), LVCFMT_LEFT, 100);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CAbnormalStatisticsDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO:  在此处添加消息处理程序代码

	//控件随程序自适应全屏后更改位置及大小
	if (1)
	{
		float fsp[2];
		POINT Newp; //获取现在对话框的大小
		CRect recta;
		GetClientRect(&recta);     //取客户区大小
		//recta = m_cRectDraw;
		Newp.x = recta.right - recta.left;
		Newp.y = recta.bottom - recta.top;
		fsp[0] = (float)Newp.x / m_potOld.x;
		fsp[1] = (float)Newp.y / m_potOld.y;
		CRect Rect;
		int woc;
		CPoint OldTLPoint, TLPoint; //左上角
		CPoint OldBRPoint, BRPoint; //右下角
		HWND  hwndChild = ::GetWindow(m_hWnd, GW_CHILD);  //列出所有控件  
		while (hwndChild)
		{
			woc = ::GetDlgCtrlID(hwndChild);//取得ID
			GetDlgItem(woc)->GetWindowRect(Rect);
			ScreenToClient(Rect);
			OldTLPoint = Rect.TopLeft();
			TLPoint.x = long(OldTLPoint.x*fsp[0]);
			TLPoint.y = long(OldTLPoint.y*fsp[1]);
			OldBRPoint = Rect.BottomRight();
			BRPoint.x = long(OldBRPoint.x *fsp[0]);
			BRPoint.y = long(OldBRPoint.y *fsp[1]);
			Rect.SetRect(TLPoint, BRPoint);
			GetDlgItem(woc)->MoveWindow(Rect, TRUE);
			hwndChild = ::GetWindow(hwndChild, GW_HWNDNEXT);
			////如果是绘图控件更新绘图区大小
			//if (woc == IDC_STATIC_DRAW)
			//{
			//	m_cRectDraw = Rect;
			//}
		}
		m_potOld = Newp;
	}
}
