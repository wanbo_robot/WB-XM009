#pragma once
#include "afxcmn.h"


// CAbnormalStatisticsDlg 对话框

class CAbnormalStatisticsDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CAbnormalStatisticsDlg)

public:
	CAbnormalStatisticsDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CAbnormalStatisticsDlg();

	//自定义区
public:
	//原始屏幕坐标
	POINT m_potOld;
	////获取控件窗口类指针
	//CWnd *m_pWnd;
	////获取绘图区参数
	//CRect m_cRectDraw;

// 对话框数据
	enum { IDD = IDD_ABNORMAL_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_listCtrl;
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
