/*****************************************************************
##	文件名称	DataBaseOperate.cpp

##	创建日期	20160504

##	版本  号	0.0.1

##	文件作者	liuming

##	文件功能	数据库操作封装，包括数据库初始化，打开本地数据库、
				关闭数据库已经对数据库的增删改查等基本操作，具体操
				作可参考各接口说明。
*****************************************************************/
#include "stdafx.h"
#include "DataBaseOperate.h"


DataBaseOperate::DataBaseOperate()
{
}


DataBaseOperate::~DataBaseOperate()
{
}


/*****************************************************************
##	接口名称	ADOConnInit

##	输入参数

##	输出参数

##	接口功能	数据库操作初始化接口，在操作数据库前需先调用该接口
				创建数据库连接并打开本地数据库操作。
*****************************************************************/
int DataBaseOperate::ADOConnInit()
{
	try
	{
		//创建ADO数据库连接操作
        m_pConnection.CreateInstance("ADODB.Connection");
        
		//打开本地SqlServer数据库agvproject
		//m_pConnection->Open("driver={SQL Server};Server=WIN-FISL5GDI94O;Database=stocksystemp;UID="";PWD=""", "", "", adModeUnknown);
		m_pConnection->Open((_bstr_t)_strDBInfo, "", "", adModeUnknown);
		
        
	}
	catch (_com_error e)
	{
		//连接失败抛出异常
		AfxMessageBox(e.Description());

		//关闭数据库连接
		ADODisConnect();

        return -1;
	}

    return 0;
}


/*****************************************************************
##	接口名称	ADODisConnect

##	输入参数

##	输出参数

##	接口功能	关闭数据库连接。
*****************************************************************/
void DataBaseOperate::ADODisConnect()
{
	//如果数据集不为空先关闭数据集
	if (m_pRecordset != NULL)
	{
		m_pRecordset->Close();
		m_pRecordset.Release(); 
		m_pRecordset = NULL; 
	}

	//断开数据库连接
	if(NULL != m_pConnection)	
	{
		m_pConnection->Close();
		m_pConnection = NULL;
	}
	
	/*if (m_pConnection->State == adStateOpen)
	{
		m_pConnection->Close();
	}*/
}


/*****************************************************************
##	接口名称		GetDataSet

##	输入参数
##	strSQL			查询语句

##	输出参数
##	m_pRecordset	查询返回记录集

##	接口功能		查询满足入参查询条件的记录集，并将查询记录集直
					接返回。
*****************************************************************/
_RecordsetPtr& DataBaseOperate::GetDataSet(CString strSQL)
{

	try
	{
		//如果当前数据库连接未打开先打开数据库连接
		if (m_pConnection == NULL)
		{
			ADOConnInit();
		}

		//使用ADO创建数据库记录集对象
		m_pRecordset.CreateInstance(__uuidof(Recordset));

		//打开并获取记录集指针
		m_pRecordset->Open((_bstr_t)strSQL,						/*查询语句*/			
							m_pConnection.GetInterfacePtr(),	/*获取数据库连接的IDispatch指针*/
							adOpenDynamic,						/*光标类型:允许用户对记录集增删改查*/
							adLockOptimistic,					/*锁定类型:修改操作时锁定当前行*/
							adCmdText);							/*指定第一个参数为SQL语句*/
	}
	catch (_com_error e)
	{
		//查询失败抛出异常
		AfxMessageBox(e.Description());

		//关闭数据库连接
		ADODisConnect();
	}
    //ADODisConnect();
	return m_pRecordset;
}

_RecordsetPtr& DataBaseOperate::GetDataSetSub(CString strSQL)
{

    try
    {
        //如果当前数据库连接未打开先打开数据库连接
        if (m_pConnection == NULL)
        {
            ADOConnInit();
        }

        //使用ADO创建数据库记录集对象
        m_pRecordsetSub.CreateInstance(__uuidof(Recordset));

        //打开并获取记录集指针
        m_pRecordsetSub->Open((_bstr_t)strSQL,						/*查询语句*/
            m_pConnection.GetInterfacePtr(),	/*获取数据库连接的IDispatch指针*/
            adOpenDynamic,						/*光标类型:允许用户对记录集增删改查*/
            adLockOptimistic,					/*锁定类型:修改操作时锁定当前行*/
            adCmdText);							/*指定第一个参数为SQL语句*/
    }
    catch (_com_error e)
    {
        //查询失败抛出异常
        AfxMessageBox(e.Description());

        //关闭数据库连接
        ADODisConnect();
    }
    //ADODisConnect();
    return m_pRecordsetSub;
}


/*****************************************************************
##	接口名称		ExecuteSQL

##	输入参数
##	strSQL			更新数据库SQL语句

##	输出参数
##	bExecFlag		更新数据库操作结果:0-成功;-1-数据库连接失败;其他值-其他错误

##	接口功能		更新数据库操作，包括对数据库进行增删改查以及
					TRUNCATE等操作。
					注：该接口执行完SQL后直接提交，为不可逆过程，
					调用前请仔细阅读接口说明并确认SQL是否正确。
*****************************************************************/
int DataBaseOperate::ExecuteSQL(CString strSQL)
{
	int iExecFlag = 1;

	try
	{
		//如果数据库连接未打开先打开数据库连接
		if (m_pConnection == NULL)
		{
			//AfxMessageBox(_T("数据库连接失败!"));
			//return -1;
			ADOConnInit();
		}
		
		//执行当前SQL语句
		m_pConnection->Execute((_bstr_t)strSQL, NULL, adCmdText);
        
		iExecFlag = 0;
	}
	catch (_com_error e)
	{
		//执行失败抛出异常信息
		CString errormessage;
		errormessage.Format("%s执行失败!\r\n错误信息:" + e.Description(), strSQL);
		//AfxMessageBox(errormessage);

		//关闭数据库连接
		ADODisConnect();
		
		iExecFlag = 1;
	}
    //ADODisConnect();
	return iExecFlag;
}

/*****************************************************************
##	接口名称		GetDataCount

##	输入参数
##	strSQL			查询语句

##	输出参数
##	bExecFlag		更新数据库操作结果:0-成功;-1-数据库连接失败;其他值-其他错误

##	接口功能		直行sql语句返回查询记录数
*****************************************************************/
int DataBaseOperate::GetDataCount(CString strSQL)
{
    int nDataCount = 0;

    try
    {
        //如果当前数据库连接未打开先打开数据库连接
        if (m_pConnection == NULL)
        {
            ADOConnInit();
        }

        //使用ADO创建数据库记录集对象
        m_pRecordset.CreateInstance(__uuidof(Recordset));

        //打开并获取记录集指针
        m_pRecordset->Open((_bstr_t)strSQL,						/*查询语句*/
            m_pConnection.GetInterfacePtr(),	/*获取数据库连接的IDispatch指针*/
            adOpenDynamic,						/*光标类型:允许用户对记录集增删改查*/
            adLockOptimistic,					/*锁定类型:修改操作时锁定当前行*/
            adCmdText);							/*指定第一个参数为SQL语句*/

        while (!(m_pRecordset->adoBOF) && !(m_pRecordset->adoEOF))
        {
            nDataCount++;
            m_pRecordset->MoveNext();
        }
    }
    catch (_com_error e)
    {
        //查询失败抛出异常
        AfxMessageBox(e.Description());

        //关闭数据库连接
       ADODisConnect();
    }
    //ADODisConnect();
    return nDataCount;
}