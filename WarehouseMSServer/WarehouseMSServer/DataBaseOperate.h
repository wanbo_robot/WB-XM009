/*****************************************************************
##	文件名称	DataBaseOperate.h

##	创建日期	20160504

##	版本号		0.0.1

##	文件作者	liuming

##	文件功能	数据库操作封装，包括数据库初始化，打开本地数据库、
				关闭数据库已经对数据库的增删改查等基本操作，具体操
				作可参考各接口说明。
*****************************************************************/
#if !defined(AFX_DataBaseOperate_H)
#define AFX_DataBaseOperate_H

//#import "C:\Program Files\Common Files\System\ado\msado15.dll" no_namespace\
//	rename("EOF", "adoEOF")rename("BOF", "adoBOF")

#import "C:\Program Files\Common Files\System\ado\msado15.dll" no_namespace\
	rename("EOF", "adoEOF")rename("BOF", "adoBOF")

#pragma once
class DataBaseOperate
{
public:
	DataBaseOperate();
	~DataBaseOperate();

public://成员变量定义区

	//定义数据库连接对象
	_ConnectionPtr m_pConnection;

	//定义数据集变量
	_RecordsetPtr m_pRecordset;
    _RecordsetPtr m_pRecordsetSub;

public://接口函数定义区

	//数据库初始化接口
	int ADOConnInit();

	//关闭数据库连接
	void ADODisConnect();

    //获取记录集数
    int GetDataCount(CString strSQL);

	//获取查询记录集
	_RecordsetPtr& GetDataSet(CString strSQL);
	
    //获取查询记录集
    _RecordsetPtr& GetDataSetSub(CString strSQL);

	//更新数据库记录（包括增删改查等操作）
	int ExecuteSQL(CString strSQL);
};

#endif

