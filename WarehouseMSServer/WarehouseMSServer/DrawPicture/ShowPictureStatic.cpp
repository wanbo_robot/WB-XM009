// ShowPictureStatic.cpp : implementation file
//

#include "stdafx.h"
#include "ShowPictureStatic.h"


// CShowPictureStatic

IMPLEMENT_DYNAMIC(CShowPictureStatic, CStatic)

CShowPictureStatic::CShowPictureStatic()
{
// 	m_iTestUpPos1 = -1;
// 	m_iTestUpPos2 = -1;
// 	m_iTestDownPos1 = -1;
// 	m_iTestDownPos2 = -1;

	resetTestPos();

	m_ptCPos.x = 0;
	m_ptCPos.y = 0;

	m_ptTPos.x = 0;
	m_ptTPos.y = 0;

	m_ptMidPos.x = 0;
	m_ptMidPos.y = 0;

	m_pShowBmp = NULL;
	m_pStartPoint  = NULL;
	m_pEndPoint    = NULL;
	m_bIsDrawLineConfig = FALSE;
}

CShowPictureStatic::~CShowPictureStatic()
{
	m_pShowBmp = NULL;
}


BEGIN_MESSAGE_MAP(CShowPictureStatic, CStatic)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()



// CShowPictureStatic message handlers


void CShowPictureStatic::resetTestPos()
{
	for (int i=0;i<4;++i)
	{
		m_iTestPos[i] = -1;
	}
}

void CShowPictureStatic::OnSize(UINT nType, int cx, int cy)
{
	CStatic::OnSize(nType, cx, cy);

}

BOOL CShowPictureStatic::OnEraseBkgnd(CDC* pDC) 
{
	if (NULL != m_pShowBmp)
	{
		BitBlt(pDC->GetSafeHdc(), 0, 0,
			m_pShowBmp->getBmpSize().cx, m_pShowBmp->getBmpSize().cy,
			m_pShowBmp->getHDC(), 0, 0, SRCCOPY);

//		if (m_bIsDrawLineConfig)
//		{
//			MoveToEx(pDC->m_hDC, m_ptCPos.x, 0, NULL);
//			LineTo(pDC->m_hDC, m_ptCPos.x, g_iImageHeight);
//
//			MoveToEx(pDC->m_hDC, m_ptMidPos.x, 0, NULL);
//			LineTo(pDC->m_hDC, m_ptMidPos.x, g_iImageHeight);
//
//			MoveToEx(pDC->m_hDC, m_ptTPos.x, 0, NULL);
//			LineTo(pDC->m_hDC, m_ptTPos.x, g_iImageHeight);
//
//			MoveToEx(pDC->m_hDC, 0, m_ptMidPos.y, NULL);
//			LineTo(pDC->m_hDC, g_iImageWidth, m_ptMidPos.y);
//
//			for (int i=0;i<4;++i)
//			{
//				MoveToEx(pDC->m_hDC, 0, m_iTestPos[i], NULL);
//				LineTo(pDC->m_hDC, g_iImageWidth, m_iTestPos[i]);
//			}
//
//// 			MoveToEx(pDC->m_hDC, 0, m_iTestUpPos1, NULL);
//// 			LineTo(pDC->m_hDC, g_iImageWidth, m_iTestUpPos1);
//// 			MoveToEx(pDC->m_hDC, 0, m_iTestUpPos2, NULL);
//// 			LineTo(pDC->m_hDC, g_iImageWidth, m_iTestUpPos2);
//// 
//// 			MoveToEx(pDC->m_hDC, 0, m_iTestDownPos1, NULL);
//// 			LineTo(pDC->m_hDC, g_iImageWidth, m_iTestDownPos1);
//// 			MoveToEx(pDC->m_hDC, 0, m_iTestDownPos2, NULL);
//// 			LineTo(pDC->m_hDC, g_iImageWidth, m_iTestDownPos2);
//			if (NULL != m_pStartPoint && NULL != m_pEndPoint)
//			{
//				MoveToEx(pDC->m_hDC, m_pStartPoint->x, m_pStartPoint->y, NULL);
//				LineTo(pDC->m_hDC, m_pEndPoint->x, m_pStartPoint->y);
//				LineTo(pDC->m_hDC, m_pEndPoint->x, m_pEndPoint->y);
//				LineTo(pDC->m_hDC, m_pStartPoint->x, m_pEndPoint->y);
//				LineTo(pDC->m_hDC, m_pStartPoint->x, m_pStartPoint->y);
//			}
//		}
	}
	return TRUE;
	return CStatic::OnEraseBkgnd(pDC);
}

void CShowPictureStatic::setShowBmp(CUnityBmp *in_pBmp)
{
	m_pShowBmp = in_pBmp;
}

void CShowPictureStatic::setSamplePosition(CPoint *in_pStartPt, CPoint *in_pEndPt)
{
	m_pStartPoint = in_pStartPt;
	m_pEndPoint   = in_pEndPt;
}
