#pragma once


// CShowPictureStatic
#include "UnityBmp.h"

class CShowPictureStatic : public CStatic
{

	DECLARE_DYNAMIC(CShowPictureStatic)

public:
	void setShowBmp(CUnityBmp *in_pBmp);

	void setSamplePosition(CPoint *in_pStartPt, CPoint *in_pEndPt);

	void resetTestPos(void);
	
	int m_iTestPos[4];    // ���Կ��ı߿�
// 	int m_iTestUpPos1;    // ���Կ����ϱ߿�
// 	int m_iTestUpPos2;    // ���Կ����ϱ߿�
// 	int m_iTestDownPos1;  // ���Կ����±߿�
// 	int m_iTestDownPos2;  // ���Կ����±߿�
	CPoint m_ptCPos;     // C��λ��
	CPoint m_ptMidPos;   // �ο���λ��
	CPoint m_ptTPos;     // T��λ��
	CPoint *m_pStartPoint;                            // ȡ����ʼ��
	CPoint *m_pEndPoint;                              // ȡ��������
	BOOL m_bIsDrawLineConfig;
public:
	CShowPictureStatic();
	virtual ~CShowPictureStatic();

protected:
	DECLARE_MESSAGE_MAP()

private:
	CUnityBmp *m_pShowBmp;                            // ��ʾͼƬ
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};


