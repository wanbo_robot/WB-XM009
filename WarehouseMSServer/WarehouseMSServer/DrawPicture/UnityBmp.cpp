#include "stdafx.h"
#include "UnityBmp.h"

CUnityBmp::CUnityBmp()
{
	m_hDC = NULL;
	m_hBmp = NULL;
	m_pData = NULL;
}

CUnityBmp::~CUnityBmp()
{
	if (m_hDC != NULL)
	{
		DeleteDC(m_hDC);
		m_hDC = NULL;
	}
	if (m_hBmp != NULL)
	{
		DeleteObject(m_hBmp);
		m_hBmp = NULL;
	}
}

HDC CUnityBmp::getHDC() const
{
	return m_hDC;
}

HBITMAP CUnityBmp::getHBitmap() const
{
	return m_hBmp;
}

BYTE *CUnityBmp::getBmpData() const
{
	return m_pData;
}

SIZE CUnityBmp::getBmpSize() const
{
	return m_Size;
}

void CUnityBmp::setBmpSize(SIZE in_Size)
{
	m_Size = in_Size;
}

SIZE &CUnityBmp::setBmpSize(void)
{
	return m_Size;
}

bool CUnityBmp::CreateUniteBmp(CUnityBmp &unityBmp)
{
	BITMAPINFO bmpInfo;
	bmpInfo.bmiHeader.biSize		= sizeof(bmpInfo);
	bmpInfo.bmiHeader.biBitCount	= 8*4;
	bmpInfo.bmiHeader.biWidth		= unityBmp.m_Size.cx;
	bmpInfo.bmiHeader.biHeight		= unityBmp.m_Size.cy;
	bmpInfo.bmiHeader.biPlanes		= 1;
	bmpInfo.bmiHeader.biCompression = BI_RGB;
	if (NULL!=unityBmp.m_hDC)
	{
		DeleteDC(unityBmp.m_hDC);
		unityBmp.m_hDC = NULL;
	}
	HDC hDesktopDC = ::GetDC(::GetDesktopWindow());
	unityBmp.m_hDC = CreateCompatibleDC(hDesktopDC);
	
	if (NULL!=unityBmp.m_hBmp)
	{
		DeleteObject(unityBmp.m_hBmp);
		unityBmp.m_hBmp = NULL;
	}
	unityBmp.m_hBmp=::CreateDIBSection(unityBmp.m_hDC,&bmpInfo,DIB_RGB_COLORS,(void**)(&unityBmp.m_pData),NULL,0);
	SelectObject(unityBmp.m_hDC,unityBmp.m_hBmp);
	::ReleaseDC(::GetDesktopWindow(),hDesktopDC);
	if (NULL!=unityBmp.m_hBmp)
	{
		return TRUE;
	}
	return FALSE;
}