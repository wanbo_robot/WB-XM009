/*
* Copyright (c) 2013, 深圳雷曼光电科技股份有限公司
* All rights reserved.
*
* 文件名称: UnityBmp.h
* 文件标识: 见配置管理计划书
* 摘 要:    简要描述本文件的内容
*
* 当前版本: V1.0
* 作 者:    Simonlay
* 添加日期: 2013.05.16
* 修改日期: 2013.05.16
* 完成日期: 2013.05.16
*/
#ifndef _LEDMAN_UNITYBMP_H_
#define _LEDMAN_UNITYBMP_H_

#include <Windows.h>


class CUnityBmp
{
public:
	static bool CreateUniteBmp(CUnityBmp &unityBmp);
	
	HDC     getHDC(void) const;
	HBITMAP getHBitmap(void) const;
	BYTE    *getBmpData(void) const;
	SIZE    getBmpSize(void) const;

	void setBmpSize(SIZE in_Size);
	SIZE &setBmpSize(void);

private:
	HDC						m_hDC;			
	HBITMAP					m_hBmp;
	BYTE*					m_pData;
	SIZE					m_Size;
public:
	CUnityBmp();
	virtual ~CUnityBmp();
};

#endif // _LEDMAN_UNITYBMP_H_