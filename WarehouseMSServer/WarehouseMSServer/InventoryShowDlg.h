#pragma once
#include "DataBaseOperate.h"

// CInventoryShowDlg 对话框

class CInventoryShowDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CInventoryShowDlg)

public:
	CInventoryShowDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CInventoryShowDlg();

public:
	//原始屏幕坐标
	POINT m_potOld;
	//获取控件窗口类指针
	CWnd *m_pWnd;
	//获取绘图区参数
	CRect m_cRectDraw;
	//线型
	int m_LineStyle;
	//线宽
	int m_LineWidth;
	//画线颜色
	COLORREF m_LineColor;
	//区域填充方式
	int m_FillStyle;
	//区域填充前景色
	COLORREF m_FillForeColor;
	//区域填充背景色
	COLORREF m_FillBackColor;

	//数据库操作变量
	DataBaseOperate m_AdoConn;

	//绘制地图
	void DrawMap();

// 对话框数据
	enum { IDD = IDD_INVENTORYSHOW_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
