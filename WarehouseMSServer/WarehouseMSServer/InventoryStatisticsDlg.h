#pragma once
#include "afxcmn.h"


// CInventoryStatisticsDlg 对话框

class CInventoryStatisticsDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CInventoryStatisticsDlg)

public:
	CInventoryStatisticsDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CInventoryStatisticsDlg();

// 对话框数据
	enum { IDD = IDD__INVENTSTATIC_DLG };

public:
	//原始屏幕坐标
	POINT m_potOld;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl m_listCtrl;
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
