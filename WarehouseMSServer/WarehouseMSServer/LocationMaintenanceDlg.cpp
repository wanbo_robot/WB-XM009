// LocationMaintenanceDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "WarehouseMSServer.h"
#include "LocationMaintenanceDlg.h"
#include "afxdialogex.h"
#include "RectangleRegion.h"

// CLocationMaintenanceDlg 对话框

IMPLEMENT_DYNAMIC(CLocationMaintenanceDlg, CDialogEx)

CLocationMaintenanceDlg::CLocationMaintenanceDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLocationMaintenanceDlg::IDD, pParent)
{
	m_bDrawFlag = false;
	m_nTransverseTrackLeft = 0;

	m_LineStyle = PS_SOLID;
	m_LineWidth = 1;
	m_LineColor = RGB(220, 220, 220);
	m_FillStyle = -1;
	m_FillForeColor = RGB(255, 255, 255);
	m_FillBackColor = RGB(255, 255, 255);
}

CLocationMaintenanceDlg::~CLocationMaintenanceDlg()
{
}

void CLocationMaintenanceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CLocationMaintenanceDlg, CDialogEx)
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BTN_LINE, &CLocationMaintenanceDlg::OnBnClickedBtnLine)
	ON_BN_CLICKED(IDC_BTN_TRANSERSE, &CLocationMaintenanceDlg::OnBnClickedBtnTranserse)
	ON_BN_CLICKED(IDC_BTN_RECTANGLE, &CLocationMaintenanceDlg::OnBnClickedBtnRectangle)
	ON_BN_CLICKED(IDC_BTN_CARD, &CLocationMaintenanceDlg::OnBnClickedBtnCard)
	ON_BN_CLICKED(IDC_BTN_EDIT, &CLocationMaintenanceDlg::OnBnClickedBtnEdit)
	ON_BN_CLICKED(IDC_BTN_SAVE, &CLocationMaintenanceDlg::OnBnClickedBtnSave)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_BTN_DRAW, &CLocationMaintenanceDlg::OnBnClickedBtnDraw)
END_MESSAGE_MAP()


// CLocationMaintenanceDlg 消息处理程序


BOOL CLocationMaintenanceDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	//保留未自适应分辨率时的应用大小
	CRect rect;
	GetClientRect(&rect);
	m_potOld.x = rect.right - rect.left;
	m_potOld.y = rect.bottom - rect.top;

	m_pWnd = GetDlgItem(IDC_STATIC_DRAW);

	//获取绘图区大小
	GetDlgItem(IDC_STATIC_DRAW)->GetWindowRect(&m_cRectDraw);
	ScreenToClient(&m_cRectDraw);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CLocationMaintenanceDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO:  在此处添加消息处理程序代码

	//控件随程序自适应全屏后更改位置及大小
	if (1)
	{
		float fsp[2];
		POINT Newp; //获取现在对话框的大小
		CRect recta;
		GetClientRect(&recta);     //取客户区大小  
		Newp.x = recta.right - recta.left;
		Newp.y = recta.bottom - recta.top;
		fsp[0] = (float)Newp.x / m_potOld.x;
		fsp[1] = (float)Newp.y / m_potOld.y;
		CRect Rect;
		int woc;
		CPoint OldTLPoint, TLPoint; //左上角
		CPoint OldBRPoint, BRPoint; //右下角
		HWND  hwndChild = ::GetWindow(m_hWnd, GW_CHILD);  //列出所有控件  
		while (hwndChild)
		{
			woc = ::GetDlgCtrlID(hwndChild);//取得ID
			GetDlgItem(woc)->GetWindowRect(Rect);
			ScreenToClient(Rect);
			OldTLPoint = Rect.TopLeft();
			TLPoint.x = long(OldTLPoint.x*fsp[0]);
			TLPoint.y = long(OldTLPoint.y*fsp[1]);
			OldBRPoint = Rect.BottomRight();
			BRPoint.x = long(OldBRPoint.x *fsp[0]);
			BRPoint.y = long(OldBRPoint.y *fsp[1]);
			Rect.SetRect(TLPoint, BRPoint);
			GetDlgItem(woc)->MoveWindow(Rect, TRUE);
			hwndChild = ::GetWindow(hwndChild, GW_HWNDNEXT);
			//如果是绘图控件更新绘图区大小
			if (woc == IDC_STATIC_DRAW)
			{
				m_cRectDraw = Rect;
			}
		}
		m_potOld = Newp;
	}

}


void CLocationMaintenanceDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO:  在此处添加消息处理程序代码
	// 不为绘图消息调用 CDialogEx::OnPaint()

	CPaintDC(this);
	CString strTextX;
	CString strTextY;
	int i;
	int j;
	int x;
	int y;

	//网格长宽赋值
	m_nLenght = m_cRectDraw.left + 22;
	m_nWide = 15;

	//使static控件区无效
	m_pWnd->Invalidate();

	//更新窗口，向系统发送重绘消息
	m_pWnd->UpdateWindow();
	//获取控件的DC指针
	CDC *pDC = m_pWnd->GetDC();
	//pDC->Rectangle(0, 0, 380, 390);

	//创建画笔
	CPen *pPenRed = new CPen();
	pPenRed->CreatePen(PS_SOLID, 1, RGB(200, 200, 200));
	CPen *pPen = NULL;

	//选中当前红色画笔，并保存以前的画笔
	CGdiObject *pOldPen = pDC->SelectObject(pPenRed);

	//绘制纵向网格线
	for (i = 0; i <= 41; i++)
	{
		pDC->MoveTo(i*(m_cRectDraw.left+22), m_cRectDraw.top - 10);
		pDC->LineTo(i*(m_cRectDraw.left + 22), m_nWide*34);
	}

	//绘制横向网格线
	for (i = 0; i <= 32; i++)
	{
		pDC->MoveTo((m_cRectDraw.left)-5, (m_cRectDraw.top-10 ) + i*15);
		pDC->LineTo((m_cRectDraw.right)-5, (m_cRectDraw.top-10 ) + i*15);
	}
	
	//网格线区域赋值
	m_cRectGrid.left = m_cRectDraw.left;
	m_cRectGrid.right = m_cRectDraw.right - 5;
	m_cRectGrid.top = m_cRectDraw.top - 10;
	m_cRectGrid.bottom = m_cRectDraw.bottom + 10;

	//左上角第一个网格区域赋值
	m_cPointFirstGrid.x = 0;
	m_cPointFirstGrid.y = m_cRectDraw.top - 10;

	/*pDC->MoveTo((m_cRectDraw.left), (m_cRectDraw.top-10)+20);
	pDC->LineTo((m_cRectDraw.right), (m_cRectDraw.top-10)+20);

	pDC->MoveTo((m_cRectDraw.left), (m_cRectDraw.top - 10) + 40);
	pDC->LineTo((m_cRectDraw.right), (m_cRectDraw.top - 10) + 40);*/

	////绘制坐标轴
	//pDC->MoveTo(m_cRectDraw.left + 10, m_cRectDraw.top + 10);
	//pDC->LineTo(m_cRectDraw.left + 10, m_cRectDraw.bottom - 10);
	//pDC->MoveTo(m_cRectDraw.left + 10, m_cRectDraw.top + 10);
	//pDC->LineTo(m_cRectDraw.right - 10, m_cRectDraw.top + 10);
	//
	////写X轴刻度值
	//for (i = 1; i <= 40; i++)
	//{
	//	strTextX.Format(_T("%d"), i);
	//	pDC->TextOutW(13+(m_cRectDraw.left + 22) * i, m_cRectDraw.top - 10, strTextX);

	//	//绘制X轴刻度
	//	pDC->MoveTo(i*(m_cRectDraw.left + 22)+20 , m_cRectDraw.top + 10);
	//	pDC->LineTo(i*(m_cRectDraw.left + 22)+20 , m_cRectDraw.top + 5);
	//}

	

	

	//恢复以前的画笔
	pDC->SelectObject(pOldPen);
	delete pPenRed;

	if (pPen != NULL)
	{
		delete pPen;
		ReleaseDC(pDC);
	}

	
}

//绘制纵向轨道
void CLocationMaintenanceDlg::OnBnClickedBtnLine()
{
	// TODO:  在此添加控件通知处理程序代码

	m_isDraw = true;
	m_nLineTransverse = 1;
	m_nLinePortrait = 0;
	m_nRectSingle = 0;
	m_nCard = 0;
}

//绘制横向轨道
void CLocationMaintenanceDlg::OnBnClickedBtnTranserse()
{
	// TODO:  在此添加控件通知处理程序代码
	m_isDraw = true;
	m_nLineTransverse = 0;
	m_nLinePortrait = 1;
	m_nRectSingle = 0;
	m_nCard = 0;
}

//绘制库位
void CLocationMaintenanceDlg::OnBnClickedBtnRectangle()
{
	// TODO:  在此添加控件通知处理程序代码
	m_isDraw = true;
	m_nLineTransverse = 0;
	m_nLinePortrait = 0;
	m_nRectSingle = 1;
	m_nCard = 0;


}

//绘制地标卡
void CLocationMaintenanceDlg::OnBnClickedBtnCard()
{
	// TODO:  在此添加控件通知处理程序代码
	m_isDraw = true;
	m_nLineTransverse = 0;
	m_nLinePortrait = 0;
	m_nRectSingle = 0;
	m_nCard = 1;
}

//编辑地图
void CLocationMaintenanceDlg::OnBnClickedBtnEdit()
{
	// TODO:  在此添加控件通知处理程序代码
	m_isDraw = false;
	m_nLineTransverse = 0;
	m_nLinePortrait = 0;
	m_nRectSingle = 0;
	m_nCard = 0;
}

//保存地图
void CLocationMaintenanceDlg::OnBnClickedBtnSave()
{
	// TODO:  在此添加控件通知处理程序代码
}


//编辑状态下鼠标左键按下消息处理函数
void CLocationMaintenanceDlg::EditLButtonDown(UINT nFlags, CPoint point)
{

}

//编辑状态下鼠标抬起消息处理函数
void CLocationMaintenanceDlg::EditLButtonUp(UINT nFlags, CPoint point)
{

}

//画图状态下鼠标左键按下消息处理函数
void CLocationMaintenanceDlg::DrawLButtonDown(UINT nFlags, CPoint point)
{

}

//画图状态下鼠标抬起消息处理函数
void CLocationMaintenanceDlg::DrawLButtonUp(UINT nFlags, CPoint point)
{
	//CBitmap bitmap;
	//CDC *pDC = m_pWnd->GetDC();
	//CRect rectLine;

	//int nXDown = 0;
	//int nYDown = 0;
	//int nXUp = 0;
	//int nYUp = 0;

	//nXDown = (m_ptDown.x - m_cRectDraw.left) / m_nLenght;
	//nYDown = (m_ptDown.y - m_cRectDraw.top) / m_nWide;

	//nXUp = (point.x - m_cRectDraw.left) / m_nLenght;
	//nYUp = (point.y - m_cRectDraw.top) / m_nWide;

	//CPoint pointDown;
	//CPoint pointUp;

	////绘制横向轨道
	//if (m_nLinePortrait)
	//{
	//	rectLine.left = nXDown*m_nLenght + (m_nLenght / 5) * 4;
	//	rectLine.right = nXUp*m_nLenght;

	//	rectLine.top = nYDown*m_nWide + (m_nWide / 5) * 2;
	//	rectLine.bottom = nYDown*m_nWide + (m_nWide / 5) * 4.5;

	//	pointDown = CPoint(rectLine.left, rectLine.bottom);
	//	pointUp = CPoint(rectLine.right, rectLine.bottom);

	//	bitmap.LoadBitmap(IDB_BITMAP_LINETRANS);
	//	CBrush brush(&bitmap);
	//	pDC->FillRect(rectLine, &brush);
	//	
	//	//绘制横向轨道数据
	//	CString strDrawMsg;
	//	strDrawMsg.Format(_T("%d,%d,%d,%d"), rectLine.left, rectLine.right, rectLine.top, rectLine.bottom);

	//	//选中区域四周各加20容差
	//	CRect rectEdit;
	//	rectEdit.left = rectLine.left - 20;
	//	rectEdit.right = rectLine.right + 20;
	//	rectEdit.top = rectLine.top - 20;
	//	rectEdit.bottom = rectLine.bottom + 20;

	//	//插入新增横向轨道图元
	//	int nRet = InserPrimitivesMsg(_T("1"), strDrawMsg, rectEdit, _T("00000"));
	//	if (0 != nRet)
	//	{
	//		return;
	//	}
	//}

	////绘制纵向轨道
	//if (m_nLineTransverse)
	//{
	//	rectLine.left = nXDown*m_nLenght + (m_nLenght / 5) * 2;
	//	rectLine.right = nXDown*m_nLenght + (m_nLenght / 5) * 4;

	//	rectLine.top = nYDown*m_nWide;
	//	rectLine.bottom = nYUp*m_nWide;

	//	pointDown = CPoint(rectLine.right, rectLine.top);
	//	pointUp = CPoint(rectLine.right, rectLine.bottom);


	//	bitmap.LoadBitmap(IDB_BITMAP_LINE);
	//	CBrush brush(&bitmap);
	//	pDC->FillRect(rectLine, &brush);

	//	//绘制纵向轨道数据
	//	CString strDrawMsg;
	//	strDrawMsg.Format(_T("%d,%d,%d,%d"), rectLine.left, rectLine.right, rectLine.top, rectLine.bottom);

	//	//选中区域四周各加20容差
	//	CRect rectEdit;
	//	rectEdit.left = rectLine.left - 20;
	//	rectEdit.right = rectLine.right + 20;
	//	rectEdit.top = rectLine.top - 20;
	//	rectEdit.bottom = rectLine.bottom + 20;

	//	//插入新增纵向轨道图元
	//	int nRet = InserPrimitivesMsg(_T("1"), strDrawMsg, rectEdit, _T("00000"));
	//	if (0 != nRet)
	//	{
	//		return;
	//	}
	//}

	////CFileFind finder;

	////释放设备环境对象
	//this->ReleaseDC(pDC);

}

//鼠标左键按下
void CLocationMaintenanceDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值

	//保存鼠标左键按下时的坐标
	m_ptDown = point;

	//处于绘图状态时调用鼠标绘图时鼠标左键按下处理函数
	if (m_isDraw)
	{
		DrawLButtonDown(nFlags, point);
	}
	//处于编辑状态时调用编辑状态鼠标左键按下处理函数
	else
	{
		EditLButtonDown(nFlags, point);
	}

	CDialogEx::OnLButtonDown(nFlags, point);
}

//鼠标左键抬起
void CLocationMaintenanceDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值

	//处于绘图状态时调用鼠标绘图时鼠标左键抬起处理函数
	if (m_isDraw)
	{
		DrawLButtonUp(nFlags, point);
	}
	//处于编辑状态时调用编辑状态鼠标左键抬起处理函数
	else
	{
		EditLButtonUp(nFlags, point);
	}

	CDialogEx::OnLButtonUp(nFlags, point);
}

//向数据库插入绘制图元数据
/****************参数说明*********************************
strTpye-图元类型：1-横向轨道；2-纵向轨道；3-库位；4-地标卡
strRect-选中区域
strDrawMsg-绘图信息字符串，每个属性中间用“,”隔开
**********************************************************/
int CLocationMaintenanceDlg::InserPrimitivesMsg(CString strTpye, CString strDrawMsg, CRect rect, CString strChildNo)
{
	CString strSQLInsert;
	strSQLInsert.Format(_T("insert into mapmsg(PrimitivesType, drawdata, editleft, editright, edittop, editbottom, state, drawtime,childno) values ('%s', '%s', %d, %d, %d, %d, '%s', getdate(), '%s')"), strTpye, strDrawMsg, rect.left, rect.right, rect.top, rect.bottom, "1", strChildNo);

	int nRet = m_AdoConn.ExecuteSQL(strSQLInsert);
	
	return nRet;
}

//生成库位时向库位信息表插入库位信息
int CLocationMaintenanceDlg::InsertLocationMsg(int nLineNum, int nColumn, int nChildColumn)
{
	CString strLineNum;
	if (nLineNum < 10)
	{
		strLineNum.Format(_T("0%d"), nLineNum);
	}
	else
	{
		strLineNum.Format(_T("%d"), nLineNum);
	}

	CString strColumn;
	if (nColumn < 10)
	{
		strColumn.Format(_T("0%d"), nColumn);
	}
	else
	{
		strColumn.Format(_T("%d"), nColumn);
	}

	CString strChildColumn;
	strChildColumn.Format(_T("%d"), nChildColumn);

	//库位编号
	CString strLocationNo;
	strLocationNo = strLineNum + strColumn;

	//子库位编号=库位编号+1位编码
	CString strChildNo;
	strChildNo.Format(_T("%s%s%d"),strLineNum, strColumn, nChildColumn);

	CString strSQLInsert;
	strSQLInsert.Format(_T("insert into locationmsg(locationno, childno, linenum, columnnum, childcolumn, locationstate, stockstate) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')"), strLocationNo, strChildNo, strLineNum, strColumn, strChildColumn, "1", "1");

	int nRet = m_AdoConn.ExecuteSQL(strSQLInsert);

	return nRet;
}

//绘制纵向轨道
void CLocationMaintenanceDlg::DrawPortraitTrack()
{
	CBitmap bitmap;
	CDC *pDC = m_pWnd->GetDC();
	CRect rectLine;
	
	rectLine.left = /*m_cRectGrid.left+*/(m_nLenght / 5) * 2;
	rectLine.right = /*m_cRectGrid.left + */(m_nLenght / 5) * 4;

	//横向轨道左侧起点为纵向轨道右侧终点
	m_nTransverseTrackLeft = rectLine.right;

	rectLine.top = m_cRectGrid.top;
	rectLine.bottom = m_cRectGrid.top+32*m_nWide;

	bitmap.LoadBitmap(IDB_BITMAP_LINE);
	CBrush brush(&bitmap);
	pDC->FillRect(rectLine, &brush);

	//绘制纵向轨道数据
	CString strDrawMsg;
	strDrawMsg.Format(_T("%d;%d;%d;%d"), rectLine.left, rectLine.right, rectLine.top, rectLine.bottom);

	//选中区域四周各加20容差
	CRect rectEdit;
	rectEdit.left = rectLine.left - 20;
	rectEdit.right = rectLine.right + 20;
	rectEdit.top = rectLine.top - 20;
	rectEdit.bottom = rectLine.bottom + 20;

	//插入新增纵向轨道图元
	int nRet = InserPrimitivesMsg(_T("2"), strDrawMsg, rectEdit, _T("00000"));
	if (0 != nRet)
	{
		return;
	}

	//释放设备环境对象
	this->ReleaseDC(pDC);
}

//绘制横向轨道
void CLocationMaintenanceDlg::DrawTransverseTrack(int nNum, int nCount)
{
	CBitmap bitmap;
	CDC *pDC = m_pWnd->GetDC();
	CRect rectLine;

	rectLine.left = m_nTransverseTrackLeft;
	rectLine.right = m_nTransverseTrackLeft + m_nLenght * nCount;

	rectLine.top = m_cRectGrid.top + nNum * m_nWide + (m_nWide / 5) * 2-1;
	rectLine.bottom = m_cRectGrid.top + nNum * m_nWide + (m_nWide / 5) * 4.5-1;

	bitmap.LoadBitmap(IDB_BITMAP_LINETRANS);
	CBrush brush(&bitmap);
	pDC->FillRect(rectLine, &brush);

	//绘制横向轨道数据
	CString strDrawMsg;
	strDrawMsg.Format(_T("%d;%d;%d;%d"), rectLine.left, rectLine.right, rectLine.top, rectLine.bottom);

	//选中区域四周各加20容差
	CRect rectEdit;
	rectEdit.left = rectLine.left - 20;
	rectEdit.right = rectLine.right + 20;
	rectEdit.top = rectLine.top - 20;
	rectEdit.bottom = rectLine.bottom + 20;

	//插入新增横向轨道图元
	int nRet = InserPrimitivesMsg(_T("1"), strDrawMsg, rectEdit, _T("00000"));
	if (0 != nRet)
	{
		return;
	}

	//CFileFind finder;

	//释放设备环境对象
	this->ReleaseDC(pDC);

}

//绘制库位
/*************************
nNum-第几排
Count-一排多少个库位
nLineNo--行号
*************************/
int CLocationMaintenanceDlg::DrawLocation(int nNum, int nCount, int nLineNo)
{
	CDC *pDC = m_pWnd->GetDC();
	
	//构造矩形区域图元对象指针
	CRectangleRegion* rectangle = new CRectangleRegion();

	CPoint ptStart;
	CPoint ptEnd;

	int nColumn = 0;
	for (int i = 2; i <= nCount*2+1; i++)
	{
		
		ptStart.x = m_cPointFirstGrid.x + i*m_nLenght/2;
		ptStart.y = m_cPointFirstGrid.y + nNum*m_nWide;

		ptEnd.x = ptStart.x + m_nLenght;
		ptEnd.y = ptStart.y + m_nWide;

		//设置控制点
		rectangle->SetStartPoint(ptStart);
		rectangle->SetEndPoint(ptEnd);

		//设置线型和填充方式
		rectangle->m_LineStyle = m_LineStyle;
		rectangle->m_LineWidth = m_LineWidth;
		rectangle->m_LineColor = m_LineColor;
		rectangle->m_FillStyle = m_FillStyle;
		rectangle->m_FillForeColor = m_FillForeColor;
		rectangle->m_FillBackColor = m_FillBackColor;

		//绘制矩形区域
		rectangle->draw(pDC);

		//绘制库位数据
		CString strDrawMsg;
		strDrawMsg.Format(_T("%d;%d;%d;%d"), ptStart.x, ptStart.y, ptEnd.x, ptEnd.y);

		
		//库位列号
		nColumn = i;
		nColumn = nColumn / 2;
		CString strColumn;
		if (nColumn < 10)
		{
			strColumn.Format(_T("0%d"), nColumn);
		}
		else
		{
			strColumn.Format(_T("%d"), nColumn);
		}

		CString strTemp;
		strTemp.Format(_T("%d"), i % 2);

		CString strLineNo;
		if (nLineNo < 10)
		{
			strLineNo.Format(_T("0%d"), nLineNo);
		}
		else
		{
			strLineNo.Format(_T("%d"), nLineNo);
		}

		//子库位编号
		CString strChildNo;
		strChildNo.Format(_T("%s%s%s"), strLineNo, strColumn, strTemp);

		//选中区域
		CRect rectEdit;
		rectEdit.left = ptStart.x;
		rectEdit.right = ptStart.x + m_nLenght / 2;
		rectEdit.top = ptStart.y;
		rectEdit.bottom = ptStart.y + m_nWide;

		//插入新库位图元
		int nRet = InserPrimitivesMsg(_T("3"), strDrawMsg, rectEdit, strChildNo);
		if (0 != nRet)
		{
			MessageBox(_T("生成库位图元失败！"));
			return -1;
		}

		//插入库位信息表
		nRet = InsertLocationMsg(nLineNo, nColumn, i % 2);
		if (0 != nRet)
		{
			MessageBox(_T("生成库位信息失败！"));
			return -1;
		}
	}
	
	//释放设备环境对象
	this->ReleaseDC(pDC);

	return 0;
}

//绘制网格线
void CLocationMaintenanceDlg::DrawGrid()
{

}

void CLocationMaintenanceDlg::OnBnClickedBtnDraw()
{
	// TODO:  在此添加控件通知处理程序代码
	m_bDrawFlag = true;

	int nRet = m_AdoConn.ADOConnInit();

	if (0 == nRet)
	{
		int nReturn = -1;

		//绘制纵向轨道
		DrawPortraitTrack();

		//绘制库位
		nReturn = DrawLocation(1, 39, 1);
		if (0 != nReturn)
		{
			return;
		}
		
		//绘制横向轨道
		DrawTransverseTrack(2, 39);
		
		//绘制库位
		nReturn = DrawLocation(3, 39, 2);
		if (0 != nReturn)
		{
			return;
		}
		nReturn = DrawLocation(4, 39, 3);
		if (0 != nReturn)
		{
			return;
		}

		//绘制横向轨道
		DrawTransverseTrack(5, 39);

		//绘制库位
		nReturn = DrawLocation(6, 39, 4);
		if (0 != nReturn)
		{
			return;
		}
		nReturn = DrawLocation(7, 39, 5);
		if (0 != nReturn)
		{
			return;
		}

		//绘制横向轨道
		DrawTransverseTrack(8, 39);

		//绘制库位
		nReturn = DrawLocation(9, 39, 6);
		if (0 != nReturn)
		{
			return;
		}
		nReturn = DrawLocation(10, 39, 7);
		if (0 != nReturn)
		{
			return;
		}

		//绘制横向轨道
		DrawTransverseTrack(11, 39);

		//绘制库位
		nReturn = DrawLocation(12, 39, 8);
		if (0 != nReturn)
		{
			return;
		}

		//绘制横向轨道
		DrawTransverseTrack(14, 36);
		//绘制库位
		nReturn = DrawLocation(15, 36, 9);
		if (0 != nReturn)
		{
			return;
		}
		nReturn = DrawLocation(16, 36, 10);
		if (0 != nReturn)
		{
			return;
		}

		//绘制横向轨道
		DrawTransverseTrack(17, 36);

		//绘制库位
		nReturn = DrawLocation(18, 36, 11);
		if (0 != nReturn)
		{
			return;
		}
		nReturn = DrawLocation(19, 36, 12);
		if (0 != nReturn)
		{
			return;
		}

		//绘制横向轨道
		DrawTransverseTrack(20, 36);

		//绘制库位
		nReturn = DrawLocation(21, 36, 13);
		if (0 != nReturn)
		{
			return;
		}
		nReturn = DrawLocation(22, 36, 14);
		if (0 != nReturn)
		{
			return;
		}

		//绘制横向轨道
		DrawTransverseTrack(23, 36);
		//绘制库位
		nReturn = DrawLocation(24, 36, 15);
		if (0 != nReturn)
		{
			return;
		}
		nReturn = DrawLocation(25, 36, 16);
		if (0 != nReturn)
		{
			return;
		}

		//绘制横向轨道
		DrawTransverseTrack(26, 36);
		//绘制库位
		nReturn = DrawLocation(27, 36, 17);
		if (0 != nReturn)
		{
			return;
		}
		nReturn = DrawLocation(28, 36, 18);
		if (0 != nReturn)
		{
			return;
		}

		//绘制横向轨道
		DrawTransverseTrack(29, 36);
		//绘制库位
		nReturn = DrawLocation(30, 36, 19);
		if (0 != nReturn)
		{
			return;
		}

		m_AdoConn.ADODisConnect();
	}
}
