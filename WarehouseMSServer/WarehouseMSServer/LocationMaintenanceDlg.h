#pragma once
#include "DataBaseOperate.h"

// CLocationMaintenanceDlg 对话框

class CLocationMaintenanceDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CLocationMaintenanceDlg)

public:
	CLocationMaintenanceDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CLocationMaintenanceDlg();

// 对话框数据
	enum { IDD = IDD_LOCATIONMAINT_DLG };

//自定义区
public:
	//原始屏幕坐标
	POINT m_potOld;
	//获取控件窗口类指针
	CWnd *m_pWnd;
	//获取绘图区参数
	CRect m_cRectDraw;
	//鼠标按下坐标
	CPoint m_ptDown;
	//网格长度
	int m_nLenght;
	//网格宽度
	int m_nWide;

	//线型
	int m_LineStyle;
	//线宽
	int m_LineWidth;
	//画线颜色
	COLORREF m_LineColor;
	//区域填充方式
	int m_FillStyle;
	//区域填充前景色
	COLORREF m_FillForeColor;
	//区域填充背景色
	COLORREF m_FillBackColor;

	//绘制图形标记
	BOOL m_bDrawFlag;
	//绘制横向轨道
	int m_nLineTransverse;
	//绘制纵向轨道
	int m_nLinePortrait;
	//绘制单排库位
	int m_nRectSingle;
	//绘制地标卡
	int m_nCard;
	//是否正在绘图
	BOOL m_isDraw;
	//横向轨道左侧起点值
	int m_nTransverseTrackLeft;
	//网格线区域
	CRect m_cRectGrid;
	//左上角坐标
	CPoint m_cPointFirstGrid;

	//数据库操作变量
	DataBaseOperate m_AdoConn;

	//编辑状态下鼠标左键按下消息处理函数
	void EditLButtonDown(UINT nFlags, CPoint point);
	//编辑状态下鼠标抬起消息处理函数
	void EditLButtonUp(UINT nFlags, CPoint point);
	//画图状态下鼠标左键按下消息处理函数
	void DrawLButtonDown(UINT nFlags, CPoint point);
	//画图状态下鼠标抬起消息处理函数
	void DrawLButtonUp(UINT nFlags, CPoint point);

	//向数据库插入绘制图元数据
	int InserPrimitivesMsg(CString strTpye, CString strDrawMsg, CRect rect, CString strChildNo);
	//生成库位时向库位信息表插入库位信息
	int InsertLocationMsg(int nLineNum, int nColumn, int nChildColumn);

	//绘制网格线
	void DrawGrid();
	//绘制纵向轨道
	void DrawTransverseTrack(int nNum, int nCount);
	//绘制纵向轨道
	void DrawPortraitTrack();
	//绘制库位
	int DrawLocation(int nNum, int nCount, int nLineNo);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnBnClickedBtnLine();
	afx_msg void OnBnClickedBtnTranserse();
	afx_msg void OnBnClickedBtnRectangle();
	afx_msg void OnBnClickedBtnCard();
	afx_msg void OnBnClickedBtnEdit();
	afx_msg void OnBnClickedBtnSave();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedBtnDraw();
};
