#include "stdafx.h"
#include "MapElement.h"


CMapElement::CMapElement()
{
	m_isSelected = false;
}


CMapElement::~CMapElement()
{
}

void CMapElement::SetStartPoint(CPoint point)
{
	m_StartPoint = point;
}

void CMapElement::SetEndPoint(CPoint point)
{
	m_EndPoint = point;
}

CPoint CMapElement::GetStartPoint()
{
	return m_StartPoint;
}

CPoint CMapElement::GetEndPoint()
{
	return m_EndPoint;
}

void CMapElement::draw(CDC *pDC)
{

}

int CMapElement::GetType()
{
	return 0;
}

CPen* CMapElement::GetPen()
{
	//构造LOGBRUSH结构
	LOGBRUSH lb;
	lb.lbColor = m_LineColor;
	lb.lbStyle = BS_SOLID;
	//返回构造的画笔指针
	return new CPen(PS_GEOMETRIC | m_LineStyle, m_LineWidth, &lb, 0, NULL);
}

CBrush* CMapElement::GetBrush()
{
	if (m_FillStyle == -1)
	{
		//填充方式值为-1，返回实心画刷指针
		return new CBrush(m_FillForeColor);
	}
	else
	{
		//否则返回阴影线画刷指针
		return new CBrush(m_FillStyle, m_FillForeColor);
	}
		
}

//设置图元包围盒
void CMapElement::SetBound()
{
	if (m_StartPoint.x < m_EndPoint.x)
	{
		m_left = m_StartPoint.x;
		m_right = m_EndPoint.x;
	}
	else
	{
		m_left = m_EndPoint.x;
		m_right = m_StartPoint.x;
	}
	if (m_StartPoint.y < m_EndPoint.y)
	{
		m_top = m_StartPoint.y;
		m_bottom = m_EndPoint.y;
	}
	else
	{
		m_top = m_EndPoint.y;
		m_bottom = m_StartPoint.y;
	}
}

//判断图元是否被选中
BOOL CMapElement::IsSelected(CPoint point)
{
	return false;
}

//绘制选中图元包围盒
void CMapElement::drawSelected(CDC *pDC)
{
	//创建空画笔
	CPen pen(PS_NULL, 1, RGB(0, 0, 0));
	//创建黑画刷
	CBrush brush(RGB(0, 0, 0));
	//选择画笔和画刷，并返回原有的画笔和画刷
	CPen* oldpen = pDC->SelectObject(&pen);
	CBrush* oldbrush = pDC->SelectObject(&brush);
	//设置绘图模式并返回原有的绘图模式
	int mode = pDC->SetROP2(R2_NOTXORPEN);
	//获得图元的控制点
	CPoint sp = GetStartPoint();
	CPoint ep = GetEndPoint();
	//用矩形区域绘制可编辑节点
	pDC->Rectangle(sp.x - 3, sp.y - 3, sp.x + 3, sp.y + 3);
	pDC->Rectangle(sp.x - 3, ep.y - 3, sp.x + 3, ep.y + 3);
	pDC->Rectangle(ep.x - 3, ep.y - 3, ep.x + 3, ep.y + 3);
	pDC->Rectangle(ep.x - 3, sp.y - 3, ep.x + 3, sp.y + 3);
	//选择原有的绘图模式
	pDC->SetROP2(mode);
	//选择原来的画笔和画刷
	pDC->SelectObject(oldpen);
	pDC->SelectObject(oldbrush);
	//删除创建的画笔和画刷
	pen.DeleteObject();
	brush.DeleteObject();
}
