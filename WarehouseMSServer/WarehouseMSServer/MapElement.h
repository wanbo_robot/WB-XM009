#pragma once
#include "afx.h"
#include <afxtempl.h>
class CMapElement :
	public CObject
{
public:
	CMapElement();
	~CMapElement();

public:
	//设置图元起始控制点
	void SetStartPoint(CPoint point);
	//设置图元终止控制点
	void SetEndPoint(CPoint point);
	//获得图元起始控制点
	CPoint GetStartPoint();
	//获得图元终止控制点
	CPoint GetEndPoint();
	//绘制图元，由具体的图元子类覆盖实现
	virtual void draw(CDC* pDC);
	//获得图元类型，由具体的图元子类覆盖实现
	virtual int GetType();
	//线型
	int m_LineStyle;
	//线宽
	int m_LineWidth;
	//画线颜色
	COLORREF m_LineColor;
	//区域填充方式
	int m_FillStyle;
	//区域填充前景色
	COLORREF m_FillForeColor;
	//区域填充背景色
	COLORREF m_FillBackColor;
	//包围盒坐标值
	int m_left, m_right, m_top, m_bottom;
	//图元是否选中标识，为true表示图元被选中
	BOOL m_isSelected;

	
	CPen* GetPen();
	CBrush* GetBrush();
	//设置图元包围盒
	void SetBound();
	//判断传入的位置坐标是否可以选中图元,如果选中则返回true
	virtual BOOL IsSelected(CPoint point);
	//绘制图元的选择标识
	virtual void drawSelected(CDC* pDC);


private:
	CPoint m_StartPoint;//图元起始控制点
	CPoint m_EndPoint;//图元终止控制点

};

