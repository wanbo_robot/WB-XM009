// PendantMaintDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "WarehouseMSServer.h"
#include "PendantMaintDlg.h"
#include "afxdialogex.h"
#include "afxdb.h"
#include "CSpreadSheet.h"

#include "QRCode\\ProductQRCode.h"
#include<string>

#include <gdiplus.h> //图象处理
using namespace Gdiplus;

// CPendantMaintDlg 对话框



IMPLEMENT_DYNAMIC(CPendantMaintDlg, CDialogEx)

CPendantMaintDlg::CPendantMaintDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPendantMaintDlg::IDD, pParent)
{
	m_strFilePathName = L"";
	m_ModulePath = L"";
	m_strPictureFileName = L"";
	m_strQRCodePic = L"";
}

CPendantMaintDlg::~CPendantMaintDlg()
{
}

void CPendantMaintDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_QRCode, m_listCtrl);
	DDX_Control(pDX, IDC_COM_SIZE, m_comSize);
	DDX_Control(pDX, IDC_COM_STATE, m_comState);
	DDX_Control(pDX, IDC_EDIT_DATA, m_editData);
	DDX_Control(pDX, IDC_EDIT_NAME, m_editName);
	DDX_Control(pDX, IDC_EDIT_NO, m_editNo);
	DDX_Control(pDX, IDC_EDIT_REMARK, m_editRemark);
	DDX_Control(pDX, IDC_EDIT_TIME, m_editTime);
	DDX_Control(pDX, IDC_EDIT_SIZE, m_editSize);
	DDX_Control(pDX, IDC_EDIT_STATE, m_editState);
	DDX_Control(pDX, IDC_STATIC_QRCODE, m_staticPicture);
}


BEGIN_MESSAGE_MAP(CPendantMaintDlg, CDialogEx)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BTN_IMPORT, &CPendantMaintDlg::OnBnClickedBtnImport)
	ON_NOTIFY(NM_CLICK, IDC_LIST_QRCode, &CPendantMaintDlg::OnNMClickListQrcode)
END_MESSAGE_MAP()


// CPendantMaintDlg 消息处理程序


BOOL CPendantMaintDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	//保留未自适应分辨率时的应用大小
	CRect rect;
	GetClientRect(&rect);
	m_potOld.x = rect.right - rect.left;
	m_potOld.y = rect.bottom - rect.top;

	////挂具尺寸下拉列表初始化
	//m_comSize.AddString(L" ");
	//m_comSize.AddString(L"大");
	//m_comSize.AddString(L"小");
	//m_comSize.SetCurSel(0);

	//初始化列表
	m_listCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_listCtrl.InsertColumn(0, _T("序号"), LVCFMT_LEFT, 50);
	m_listCtrl.InsertColumn(1, _T("挂具编码"), LVCFMT_LEFT, 150);
	m_listCtrl.InsertColumn(2, _T("挂具名称"), LVCFMT_LEFT, 150);
	m_listCtrl.InsertColumn(3, _T("挂件规格"), LVCFMT_LEFT, 150);
	m_listCtrl.InsertColumn(4, _T("入厂时间"), LVCFMT_LEFT, 100);
	m_listCtrl.InsertColumn(5, _T("状态"), LVCFMT_LEFT, 80);
	m_listCtrl.InsertColumn(6, _T("尺寸"), LVCFMT_LEFT, 100);
	m_listCtrl.InsertColumn(7, _T("备注"), LVCFMT_LEFT, 200);
	
	//列表数据初始化
	QRCodeListInit();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CPendantMaintDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO:  在此处添加消息处理程序代码

	//控件随程序自适应全屏后更改位置及大小
	if (1)
	{
		float fsp[2];
		POINT Newp; //获取现在对话框的大小
		CRect recta;
		GetClientRect(&recta);     //取客户区大小
		//recta = m_cRectDraw;
		Newp.x = recta.right - recta.left;
		Newp.y = recta.bottom - recta.top;
		fsp[0] = (float)Newp.x / m_potOld.x;
		fsp[1] = (float)Newp.y / m_potOld.y;
		CRect Rect;
		int woc;
		CPoint OldTLPoint, TLPoint; //左上角
		CPoint OldBRPoint, BRPoint; //右下角
		HWND  hwndChild = ::GetWindow(m_hWnd, GW_CHILD);  //列出所有控件  
		while (hwndChild)
		{
			woc = ::GetDlgCtrlID(hwndChild);//取得ID
			GetDlgItem(woc)->GetWindowRect(Rect);
			ScreenToClient(Rect);
			OldTLPoint = Rect.TopLeft();
			TLPoint.x = long(OldTLPoint.x*fsp[0]);
			TLPoint.y = long(OldTLPoint.y*fsp[1]);
			OldBRPoint = Rect.BottomRight();
			BRPoint.x = long(OldBRPoint.x *fsp[0]);
			BRPoint.y = long(OldBRPoint.y *fsp[1]);
			Rect.SetRect(TLPoint, BRPoint);
			GetDlgItem(woc)->MoveWindow(Rect, TRUE);
			hwndChild = ::GetWindow(hwndChild, GW_HWNDNEXT);
			////如果是绘图控件更新绘图区大小
			//if (woc == IDC_STATIC_DRAW)
			//{
			//	m_cRectDraw = Rect;
			//}
		}
		m_potOld = Newp;
	}
}

//批量导入挂具信息
void CPendantMaintDlg::OnBnClickedBtnImport()
{
	// TODO:  在此添加控件通知处理程序代码

	//清空列表
	m_listCtrl.DeleteAllItems();

	BOOL isOpen = TRUE;		//是否打开(否则为保存)
	CString defaultDir = L"";	//默认打开的文件路径
	CString fileName = L"";			//默认打开的文件名
	CString filter = L"文件 (*.xls; *.xlsx)|*.xls;*.xlsx||";	//文件过虑的类型
	CFileDialog openFileDlg(isOpen, defaultDir, fileName, OFN_HIDEREADONLY | OFN_READONLY, filter, NULL);
	openFileDlg.GetOFN().lpstrInitialDir = L"";
	INT_PTR result = openFileDlg.DoModal();
	CString filePath = defaultDir;
	if (result == IDOK) 
	{
		filePath = openFileDlg.GetPathName();
	}

	CSpreadSheet SS(filePath, L"Sheet1");
	CStringArray Rows, Column;
	
	int nIndex = 0;
	for (int i = 2; i <= SS.GetTotalRows(); i++)
	{
		// 读取一行
		SS.ReadRow(Rows, i);
		CString strContents = L"";
		CString strNum;
		

		CString strPendantNo;
		CString strPendantname;
		CString strPendantmode;
		CString strIntime;
		CString strPendantstate;
		CString strPendantsize;
		CString strRemark;

		strPendantNo = Rows.GetAt(0);
		strPendantname = Rows.GetAt(1);
		strPendantmode = Rows.GetAt(2);
		strIntime = Rows.GetAt(3);
		strPendantstate = Rows.GetAt(4);
		strPendantsize = Rows.GetAt(5);
		strRemark = Rows.GetAt(6);

		strNum.Format(L"%d", nIndex);
		m_listCtrl.InsertItem(nIndex, strNum);
		m_listCtrl.SetItemText(nIndex, 1, strPendantNo);
		m_listCtrl.SetItemText(nIndex, 2, strPendantname);
		m_listCtrl.SetItemText(nIndex, 3, strPendantmode);
		m_listCtrl.SetItemText(nIndex, 4, strIntime);
		m_listCtrl.SetItemText(nIndex, 5, strPendantstate);
		m_listCtrl.SetItemText(nIndex, 6, strPendantsize);
		m_listCtrl.SetItemText(nIndex, 7, strRemark);

		
		CString strSQLInsert;
		strSQLInsert.Format(_T("insert into pendantmsg(PendantNo, pendantname, pendantmode, intime, pendantstate, pendantsize, remark) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')"), strPendantNo, strPendantname, strPendantmode, strIntime, strPendantstate, strPendantsize, strRemark);

		int nRet = m_AdoConn.ExecuteSQL(strSQLInsert);

		if (nRet != 0)
		{
			MessageBox(L"挂具信息导入失败!");
			return;
		}
		
		nIndex++;
	}
}


CString CPendantMaintDlg::GetExcelDriver()
{

	wchar_t szBuf[2001];
	wchar_t excl[] = L"Excel";
	WORD cbBufMax = 2000;
	WORD cbBufOut;
	wchar_t *pszBuf = szBuf;
	CString sDriver;
	// 获取已安装驱动的名称(函数在odbcinst.h里)
	if (!SQLGetInstalledDrivers(szBuf, cbBufMax, &cbBufOut))
		return L"";
	// 检索已安装的驱动是否有Excel...
	// AfxMessageBox(CString(pszBuf));
	do
	{
		if (wcsstr(pszBuf, excl) != 0)
		{
			//发现 !
			sDriver = CString(pszBuf);
			break;
		}
		wchar_t ze = { '\0' };
		pszBuf = wcschr(pszBuf, ze) + 1;
	} while (pszBuf[1] != '\0');

	return sDriver;

	
}

void CPendantMaintDlg::OnNMClickListQrcode(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO:  在此添加控件通知处理程序代码
	*pResult = 0;

	POSITION ps;
	int nIndex;

	ps = m_listCtrl.GetFirstSelectedItemPosition();
	nIndex = m_listCtrl.GetNextSelectedItem(ps);

	//未选中行返回
	if (nIndex == -1)
	{
		return;
	}

	m_listCtrl.SetItemState(nIndex, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);

	CString strPendantNo;
	CString strPendantname;
	CString strPendantmode;
	CString strIntime;
	CString strPendantstate;
	CString strPendantsize;
	CString strRemark;

	strPendantNo = m_listCtrl.GetItemText(nIndex, 1);
	strPendantname = m_listCtrl.GetItemText(nIndex, 2);
	strPendantmode = m_listCtrl.GetItemText(nIndex, 3);
	strIntime = m_listCtrl.GetItemText(nIndex, 4);
	strPendantstate = m_listCtrl.GetItemText(nIndex, 5);
	strPendantsize = m_listCtrl.GetItemText(nIndex, 6);
	strRemark = m_listCtrl.GetItemText(nIndex, 7);

	m_editNo.SetWindowTextW(strPendantNo);
	m_editName.SetWindowTextW(strPendantname);
	m_editData.SetWindowTextW(strPendantmode);
	m_editTime.SetWindowTextW(strIntime);
	m_editState.SetWindowTextW(strPendantstate);
	m_editSize.SetWindowTextW(strPendantsize);
	m_editRemark.SetWindowTextW(strRemark);
	
	QRCodeGenerate(strPendantNo);

}

//列表初始化
void CPendantMaintDlg::QRCodeListInit()
{
	//清空列表
	m_listCtrl.DeleteAllItems();

	CString strSQL;

	strSQL = L"select PendantNo, pendantname, pendantmode, intime, isnull(pendantstate, ' ') pendantstate, pendantsize, isnull(remark, ' ') remark from pendantmsg order by pendantname";

	//查询当前条件是否有记录
	m_AdoConn.m_pRecordset = m_AdoConn.GetDataSet(strSQL);

	int nIndex = 0;
	CString strNum;
	while (!m_AdoConn.m_pRecordset->adoEOF)
	{
		CString strPendantNo;
		CString strPendantname;
		CString strPendantmode;
		CString strIntime;
		CString strPendantstate;
		CString strPendantsize;
		CString strRemark;

		strPendantNo = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("PendantNo");
		strPendantname = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("pendantname");
		strPendantmode = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("pendantmode");
		strIntime = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("intime");
		strPendantstate = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("pendantstate");
		strPendantsize = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("pendantsize");
		strRemark = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("remark");

		strNum.Format(L"%d", nIndex);
		m_listCtrl.InsertItem(nIndex, strNum);
		m_listCtrl.SetItemText(nIndex, 1, strPendantNo);
		m_listCtrl.SetItemText(nIndex, 2, strPendantname);
		m_listCtrl.SetItemText(nIndex, 3, strPendantmode);
		m_listCtrl.SetItemText(nIndex, 4, strIntime);
		m_listCtrl.SetItemText(nIndex, 5, strPendantstate);
		m_listCtrl.SetItemText(nIndex, 6, strPendantsize);
		m_listCtrl.SetItemText(nIndex, 7, strRemark);

		nIndex++;

		m_AdoConn.m_pRecordset->MoveNext();
	}

}

//二维码接口
void CPendantMaintDlg::QRCodeGenerate(CString strCodeMsg)
{
	m_strQRCodeMsg = strCodeMsg;
	UpdateData();
	m_strQRCodeMsg.TrimLeft(_T(" "));
	m_strQRCodeMsg.TrimRight(_T(" "));
	if (m_strQRCodeMsg.IsEmpty())
	{
		MessageBox(_T("挂件编码读取失败!"));
		return;
	}

	int scale = 4;
	

	if (m_strFilePathName.IsEmpty())
	{
		CString strPahtName = GetModulePath();
		strPahtName += "\\GY.bmp";
		m_strPictureFileName = strPahtName;
		m_strFilePathName = m_strPictureFileName;
		UpdateData(FALSE);
	}
	// 编码

	if (gyProductQRCode(scale, m_strQRCodeMsg.GetBuffer(0), m_strFilePathName.GetBuffer(0)))
	{
		ShowPicture(m_strPictureFileName);
		m_strQRCodePic = m_strFilePathName;
		UpdateData(FALSE);
		//	MessageBox(_T("处理完成!"));		
	}
	else
	{
		MessageBox(_T("错误!"));
	}
}

void CPendantMaintDlg::ShowPicture(CString strFilePathName)
{
	int cx, cy;
	CImage image;
	CRect rect;

	image.Load(strFilePathName);

	cx = image.GetWidth();
	cy = image.GetHeight();

	//获取Picture Control控件的大小  
	GetDlgItem(IDC_STATIC_QRCODE)->GetWindowRect(&rect);
	//将客户区选中到控件表示的矩形区域内  
	ScreenToClient(&rect);

	//窗口移动到控件表示的区域  
	GetDlgItem(IDC_STATIC_QRCODE)->MoveWindow(rect.left, rect.top, cx, cy, TRUE);
	CWnd *pWnd = NULL;
	pWnd = GetDlgItem(IDC_STATIC_QRCODE);//获取控件句柄  
	pWnd->GetClientRect(&rect);//获取句柄指向控件区域的大小  

	CDC *pDc = NULL;
	pDc = pWnd->GetDC();//获取picture的DC  

	image.Draw(pDc->m_hDC, rect);//将图片绘制到picture表示的区域内  

	ReleaseDC(pDc);

	//先得到图片的大小
	//int nWidth = 0, nHeiht = 0;
	//if (!GetBMPSize(strFilePathName, nWidth, nHeiht))
	//	return;
	//CRect tmp_rcPictureRect;
	//m_staticPicture.GetWindowRect(tmp_rcPictureRect);
	//ScreenToClient(tmp_rcPictureRect);
	//tmp_rcPictureRect.right = tmp_rcPictureRect.left + nWidth;
	//tmp_rcPictureRect.bottom = tmp_rcPictureRect.top + nHeiht;
	//m_staticPicture.MoveWindow(tmp_rcPictureRect);//控件的大小位置

	//m_oShowBmp.setBmpSize().cx = tmp_rcPictureRect.Width();
	//m_oShowBmp.setBmpSize().cy = tmp_rcPictureRect.Height();
	//SIZE newSize;
	//newSize.cx = tmp_rcPictureRect.Width();
	//newSize.cy = tmp_rcPictureRect.Height();
	////	m_oShowBmp.setBmpSize(newSize);//这个也没用
	//CUnityBmp::CreateUniteBmp(m_oShowBmp);//图象大小。重新设置

	//BSTR tmp_bstrPictureFile = strFilePathName.AllocSysString();
	//Bitmap tmp_Bitmap(tmp_bstrPictureFile);
	//Graphics tmp_Graphics(m_oShowBmp.getHDC());

	//tmp_Graphics.DrawImage(&tmp_Bitmap, 0, 0, m_oShowBmp.getBmpSize().cx, m_oShowBmp.getBmpSize().cy);

	//SysFreeString(tmp_bstrPictureFile);
	//m_staticPicture.SetBitmap((HBITMAP)tmp_Bitmap);
	//m_staticPicture.Invalidate(TRUE);
}

bool CPendantMaintDlg::GetBMPSize(CString strFilePathName, int & nWidth, int & nHeiht)
{
	BITMAPINFOHEADER bitmapinfoheader;
	FILE * stream;
	memset(&bitmapinfoheader, 0, sizeof(BITMAPINFOHEADER));
	USES_CONVERSION;
	std::string strFileName = T2A(strFilePathName.GetBuffer(0));

	stream = fopen(strFileName.c_str(), "r");
	fseek(stream, 14, 1);
	fread((char*)&bitmapinfoheader, sizeof(BITMAPINFOHEADER), 1, stream);
	fclose(stream);
	nHeiht = bitmapinfoheader.biHeight;
	nWidth = bitmapinfoheader.biWidth;
	return true;
}

//得到运行的路径
CString CPendantMaintDlg::GetModulePath()
{
	CString sPath;
	sPath.Empty();
	//获取程序路径名
	GetModuleFileName(NULL, sPath.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
	sPath.ReleaseBuffer();

	//去掉名称，获取路径
	int nPos;
	nPos = sPath.ReverseFind(_T('\\'));
	if (nPos != -1)
		sPath = sPath.Left(nPos + 1);
	sPath.TrimRight(_T("\\"));
	sPath.TrimRight(_T("/"));
	m_ModulePath = sPath;
	return m_ModulePath;
}