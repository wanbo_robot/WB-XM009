#pragma once
#include "afxcmn.h"
#include "DataBaseOperate.h"
#include "afxwin.h"
#include "DrawPicture\\ShowPictureStatic.h"

// CPendantMaintDlg 对话框


class CPendantMaintDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CPendantMaintDlg)

public:
	CPendantMaintDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CPendantMaintDlg();

public:
	//原始屏幕坐标
	POINT m_potOld;

	//数据库操作变量
	DataBaseOperate m_AdoConn;

	//二维码信息
	CString m_strQRCodeMsg;
	CString m_strFilePathName;
	CString m_ModulePath;
	CString m_strPictureFileName;
	CString m_strQRCodePic;
	CUnityBmp m_oShowBmp;//位图

//自定义区
public:
	//列表初始化
	void QRCodeListInit();
	//二维码生成接口
	void QRCodeGenerate(CString strCodeMsg);
	//显示二维码
	void ShowPicture(CString strFilePathName);
	//获取图像大小
	bool GetBMPSize(CString strFilePathName, int & nWidth, int & nHeiht);
	//得到运行的路径
	CString GetModulePath();

	//获取excel驱动
	CString GetExcelDriver();
// 对话框数据
	enum { IDD = IDD_PENDANTMAINT_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl m_listCtrl;
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedBtnImport();
	afx_msg void OnNMClickListQrcode(NMHDR *pNMHDR, LRESULT *pResult);
	CComboBox m_comSize;
	CComboBox m_comState;
	CEdit m_editData;
	CEdit m_editName;
	CEdit m_editNo;
	CEdit m_editRemark;
	CEdit m_editTime;
	CEdit m_editSize;
	CEdit m_editState;
	CStatic m_staticPicture;
};
