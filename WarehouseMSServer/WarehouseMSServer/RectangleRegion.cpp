#include "stdafx.h"
#include "RectangleRegion.h"


CRectangleRegion::CRectangleRegion()
{
}


CRectangleRegion::~CRectangleRegion()
{
}

void CRectangleRegion::draw(CDC *pDC)
{
	////获得矩形区域控制点
	//CPoint sp = GetStartPoint();
	//CPoint ep = GetEndPoint();
	////绘制矩形区域
	//pDC->Rectangle(sp.x, sp.y, ep.x, ep.y);

	//设置绘图模式为使用画笔定义的颜色
	pDC->SetROP2(R2_COPYPEN);
	//获得当前线型画笔
	CPen* pen = GetPen();
	//获得当前填充模式的画刷
	CBrush* brush = GetBrush();
	//如果是阴影线画刷，设置背景色
	if (m_FillStyle != -1)
	{
		pDC->SetBkColor(m_FillBackColor);
	}
	//选择画笔，并返回原有画笔
	CPen* oldpen = pDC->SelectObject(pen);
	//选择画刷，并返回原有画刷
	CBrush* oldbrush = pDC->SelectObject(brush);
	//获得矩形区域控制点
	CPoint sp = GetStartPoint();
	CPoint ep = GetEndPoint();
	//绘制矩形区域
	pDC->Rectangle(sp.x, sp.y, ep.x, ep.y);
	//选择回原有画笔和画刷
	pDC->SelectObject(oldpen);
	pDC->SelectObject(oldbrush);
	//删除创建的画笔和画刷
	pen->DeleteObject(); 
	delete pen;
	brush->DeleteObject(); 
	delete brush;

}

int CRectangleRegion::GetType()
{
	//返回图元类型为矩形区域
	return 4;
}
