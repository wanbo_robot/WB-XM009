#pragma once
#include "MapElement.h"
class CRectangleRegion :
	public CMapElement
{
public:
	CRectangleRegion();
	~CRectangleRegion();

	void draw(CDC* pDC);//绘制图元
	int GetType();//返回图元类型

};

