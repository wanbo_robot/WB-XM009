#pragma once
#include "afxcmn.h"


// CReportManageDlg 对话框

class CReportManageDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CReportManageDlg)

public:
	CReportManageDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CReportManageDlg();

public:
	//原始屏幕坐标
	POINT m_potOld;

// 对话框数据
	enum { IDD = IDD_REPORTMAINAGE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_listCtrl;
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
