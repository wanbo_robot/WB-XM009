// SystemRunDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "WarehouseMSServer.h"
#include "SystemRunDlg.h"
#include "afxdialogex.h"
#include "RectangleRegion.h"


// CSystemRunDlg 对话框

IMPLEMENT_DYNAMIC(CSystemRunDlg, CDialogEx)

CSystemRunDlg::CSystemRunDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSystemRunDlg::IDD, pParent)
{
	m_LineStyle = PS_SOLID;
	m_LineWidth = 1;
	m_LineColor = RGB(220, 220, 220);
	m_FillStyle = -1;
	m_FillForeColor = RGB(255, 255, 255);
	m_FillBackColor = RGB(255, 255, 255);
}

CSystemRunDlg::~CSystemRunDlg()
{
}

void CSystemRunDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSystemRunDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CSystemRunDlg 消息处理程序


BOOL CSystemRunDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	//保留未自适应分辨率时的应用大小
	CRect rect;
	GetClientRect(&rect);
	m_potOld.x = rect.right - rect.left;
	m_potOld.y = rect.bottom - rect.top;


	m_pWnd = GetDlgItem(IDC_STATIC_DRAW);

	//获取绘图区大小
	GetDlgItem(IDC_STATIC_DRAW)->GetWindowRect(&m_cRectDraw);
	ScreenToClient(&m_cRectDraw);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}

//绘制地图
void CSystemRunDlg::DrawMap()
{
	//构造矩形区域图元对象指针
	CRectangleRegion* rectangle = new CRectangleRegion();

	CDC *pDC = m_pWnd->GetDC();

	//读取地图中轨道信息
	CString strSQL;
	strSQL = _T("select PrimitivesType ,drawdata from mapmsg where PrimitivesType != '3'");
	
	//查询当前条件是否有记录
	m_AdoConn.m_pRecordset = m_AdoConn.GetDataSet(strSQL);
	
	while (!m_AdoConn.m_pRecordset->adoEOF)
	{
		CString strPrimitivesType;
		CString strDrawData;

		strPrimitivesType = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("PrimitivesType");
		strDrawData = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("drawdata");
		
		CString strLeft;
		CString strRight;
		CString strTop;
		CString strBottom;
		AfxExtractSubString(strLeft, strDrawData, 0, ';');
		AfxExtractSubString(strRight, strDrawData, 1, ';');
		AfxExtractSubString(strTop, strDrawData, 2, ';');
		AfxExtractSubString(strBottom, strDrawData, 3, ';');

		int nLeft = _ttoi(strLeft);
		int nRight = _ttoi(strRight);
		int nTop = _ttoi(strTop);
		int nBottom = _ttoi(strBottom);

		CRect rectLine;
		rectLine.left = nLeft;
		rectLine.right = nRight;
		rectLine.top = nTop;
		rectLine.bottom = nBottom;

		
		CBitmap bitmap;
		//绘制纵向轨道
		if (strPrimitivesType == _T("2"))
		{
			bitmap.LoadBitmap(IDB_BITMAP_LINE);
		}//绘制横向轨道
		else if (strPrimitivesType == _T("1"))
		{
			bitmap.LoadBitmap(IDB_BITMAP_LINETRANS);
		}

		CBrush brush(&bitmap);
		pDC->FillRect(rectLine, &brush);

		m_AdoConn.m_pRecordset->MoveNext();
	}

	strSQL = _T("select a.drawdata, b.locationstate, b.stockstate from mapmsg a, locationmsg b where a.childno = b.childno and a.state = '1' and a.PrimitivesType= '3' order by b.childno");

	//查询当前条件是否有记录
	m_AdoConn.m_pRecordset = m_AdoConn.GetDataSet(strSQL);

	while (!m_AdoConn.m_pRecordset->adoEOF)
	{
		CString strDrawData;
		CString strLocationState;
		CString strStockState;
		
		strDrawData = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("drawdata");
		strLocationState = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("locationstate");
		strStockState = (LPCTSTR)(_bstr_t)m_AdoConn.m_pRecordset->GetCollect("stockstate");

		CString strStartX;
		CString strStartY;
		CString strEndX;
		CString strEndY;
		
		AfxExtractSubString(strStartX, strDrawData, 0, ';');
		AfxExtractSubString(strStartY, strDrawData, 1, ';');
		AfxExtractSubString(strEndX, strDrawData, 2, ';');
		AfxExtractSubString(strEndY, strDrawData, 3, ';');
	
		CPoint ptStart(_ttoi(strStartX), _ttoi(strStartY));
		CPoint ptEnd(_ttoi(strEndX), _ttoi(strEndY));

		//设置控制点
		rectangle->SetStartPoint(ptStart);
		rectangle->SetEndPoint(ptEnd);

		if (strLocationState == _T("0"))
		{
			m_FillForeColor = RGB(228, 0, 0);
			m_FillBackColor = RGB(228, 0, 0);
		}
		else if (strLocationState == _T("1"))
		{
			if (strStockState == _T("0"))
			{
				m_FillForeColor = RGB(43, 162, 69);
				m_FillBackColor = RGB(43, 162, 69);
			}
			else if (strStockState == _T("1"))
			{
				m_FillForeColor = RGB(200, 200, 200);
				m_FillBackColor = RGB(200, 200, 200);
			}
			
		}
		//设置线型和填充方式
		rectangle->m_LineStyle = m_LineStyle;
		rectangle->m_LineWidth = m_LineWidth;
		rectangle->m_LineColor = m_LineColor;
		rectangle->m_FillStyle = m_FillStyle;
		rectangle->m_FillForeColor = m_FillForeColor;
		rectangle->m_FillBackColor = m_FillBackColor;

		//绘制矩形区域
		rectangle->draw(pDC);

		m_AdoConn.m_pRecordset->MoveNext();
	}
	/*else
	{
		MessageBox(_T("数据库连接失败！"));
	}*/

	//释放设备环境对象
	this->ReleaseDC(pDC);
}

void CSystemRunDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO:  在此处添加消息处理程序代码
	// 不为绘图消息调用 CDialogEx::OnPaint()

	//初始化地图
	DrawMap();
}


void CSystemRunDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO:  在此处添加消息处理程序代码

	//控件随程序自适应全屏后更改位置及大小
	if (1)
	{
		float fsp[2];
		POINT Newp; //获取现在对话框的大小
		CRect recta;
		GetClientRect(&recta);     //取客户区大小
		//recta = m_cRectDraw;
		Newp.x = recta.right - recta.left;
		Newp.y = recta.bottom - recta.top;
		fsp[0] = (float)Newp.x / m_potOld.x;
		fsp[1] = (float)Newp.y / m_potOld.y;
		CRect Rect;
		int woc;
		CPoint OldTLPoint, TLPoint; //左上角
		CPoint OldBRPoint, BRPoint; //右下角
		HWND  hwndChild = ::GetWindow(m_hWnd, GW_CHILD);  //列出所有控件  
		while (hwndChild)
		{
			woc = ::GetDlgCtrlID(hwndChild);//取得ID
			GetDlgItem(woc)->GetWindowRect(Rect);
			ScreenToClient(Rect);
			OldTLPoint = Rect.TopLeft();
			TLPoint.x = long(OldTLPoint.x*fsp[0]);
			TLPoint.y = long(OldTLPoint.y*fsp[1]);
			OldBRPoint = Rect.BottomRight();
			BRPoint.x = long(OldBRPoint.x *fsp[0]);
			BRPoint.y = long(OldBRPoint.y *fsp[1]);
			Rect.SetRect(TLPoint, BRPoint);
			GetDlgItem(woc)->MoveWindow(Rect, TRUE);
			hwndChild = ::GetWindow(hwndChild, GW_HWNDNEXT);
			//如果是绘图控件更新绘图区大小
			if (woc == IDC_STATIC_DRAW)
			{
				m_cRectDraw = Rect;
			}
		}
		m_potOld = Newp;
	}
}
