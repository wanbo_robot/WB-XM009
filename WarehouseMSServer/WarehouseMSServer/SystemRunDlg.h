#pragma once
#include "DataBaseOperate.h"

// CSystemRunDlg 对话框

class CSystemRunDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSystemRunDlg)

public:
	CSystemRunDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSystemRunDlg();

//自定义区
public:
	//原始屏幕坐标
	POINT m_potOld;
	//获取控件窗口类指针
	CWnd *m_pWnd;
	//获取绘图区参数
	CRect m_cRectDraw;
	//线型
	int m_LineStyle;
	//线宽
	int m_LineWidth;
	//画线颜色
	COLORREF m_LineColor;
	//区域填充方式
	int m_FillStyle;
	//区域填充前景色
	COLORREF m_FillForeColor;
	//区域填充背景色
	COLORREF m_FillBackColor;

	//数据库操作变量
	DataBaseOperate m_AdoConn;

	//绘制地图
	void DrawMap();

// 对话框数据
	enum { IDD = IDD_SYSTEMRUD_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
