
// WarehouseMSServerDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "WarehouseMSServer.h"
#include "WarehouseMSServerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CWarehouseMSServerDlg 对话框



CWarehouseMSServerDlg::CWarehouseMSServerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CWarehouseMSServerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWarehouseMSServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_BTN_RUN, m_btnSystemRun);
	DDX_Control(pDX, IDC_BTN_SHOW, m_btnInventoryShow);
	DDX_Control(pDX, IDC_BTN_INVENTSTAT, m_btnInventStat);
	DDX_Control(pDX, IDC_BTN_MAINTAIN, m_btnMaintain);
	DDX_Control(pDX, IDC_BTN_REPORT, m_btnReport);
	DDX_Control(pDX, IDC__BTN_ABNORMAL, m_btnAbnormal);
	DDX_Control(pDX, IDC_BTN_PENDANTNO, m_btnPendant);
}

BEGIN_MESSAGE_MAP(CWarehouseMSServerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BTN_RUN, &CWarehouseMSServerDlg::OnBnClickedBtnRun)
	ON_BN_CLICKED(IDC_BTN_SHOW, &CWarehouseMSServerDlg::OnBnClickedBtnShow)
	ON_BN_CLICKED(IDC_BTN_INVENTSTAT, &CWarehouseMSServerDlg::OnBnClickedBtnInventstat)
	ON_BN_CLICKED(IDC_BTN_MAINTAIN, &CWarehouseMSServerDlg::OnBnClickedBtnMaintain)
	ON_BN_CLICKED(IDC_BTN_REPORT, &CWarehouseMSServerDlg::OnBnClickedBtnReport)
	ON_BN_CLICKED(IDC__BTN_ABNORMAL, &CWarehouseMSServerDlg::OnClickedBtnAbnormal)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_PENDANTNO, &CWarehouseMSServerDlg::OnBnClickedBtnPendantno)
END_MESSAGE_MAP()


// CWarehouseMSServerDlg 消息处理程序

BOOL CWarehouseMSServerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO:  在此添加额外的初始化代码

	//获取数据库连接串
	GetDBInfo();

	m_BkGndBmp.LoadBitmapW(IDB_BITMAP_BACKGROUD);

	//保留未自适应分辨率时的应用大小
	CRect rect;
	GetClientRect(&rect);
	m_potOld.x = rect.right - rect.left;
	m_potOld.y = rect.bottom - rect.top;

	//程序自适应全屏
	if (1)
	{
		WINDOWPLACEMENT m_struOldWndpl;
		//get current system resolution
		int g_iCurScreenWidth = GetSystemMetrics(SM_CXSCREEN);
		int g_iCurScreenHeight = GetSystemMetrics(SM_CYSCREEN);

		//for full screen while backplay
		GetWindowPlacement(&m_struOldWndpl);

		CRect rectWholeDlg;//entire client(including title bar)
		CRect rectClient;//client area(not including title bar)
		CRect rectFullScreen;
		GetWindowRect(&rectWholeDlg);
		RepositionBars(0, 0xffff, AFX_IDW_PANE_FIRST, reposQuery, &rectClient);
		ClientToScreen(&rectClient);

		rectFullScreen.left = rectWholeDlg.left - rectClient.left;
		rectFullScreen.top = rectWholeDlg.top - rectClient.top;
		rectFullScreen.right = rectWholeDlg.right + g_iCurScreenWidth - rectClient.right;
		rectFullScreen.bottom = rectWholeDlg.bottom + g_iCurScreenHeight - rectClient.bottom;
		//enter into full screen;
		WINDOWPLACEMENT struWndpl;
		struWndpl.length = sizeof(WINDOWPLACEMENT);
		struWndpl.flags = 0;
		struWndpl.showCmd = SW_SHOWNORMAL;
		struWndpl.rcNormalPosition = rectFullScreen;
		SetWindowPlacement(&struWndpl);
	}

	//初始化设置导航菜单按钮状态
	if (1)
	{
		//系统默认显示系统运行界面，初始化系统运行按钮不可用
		m_btnSystemRun.EnableWindow(FALSE);
		m_btnInventoryShow.EnableWindow(TRUE);
		m_btnInventStat.EnableWindow(TRUE);
		m_btnMaintain.EnableWindow(TRUE);
		m_btnReport.EnableWindow(TRUE);
		m_btnAbnormal.EnableWindow(TRUE);
		m_btnPendant.EnableWindow(TRUE);
	}

	//设置界面按钮背景
	if (1)
	{
		m_btnOK.SetSkin(IDB_BITMAP_CLOSE1, IDB_BITMAP_CLOSE2, IDB_BITMAP_CLOSE2, 0, 0, 0, 0, 0, 0);
		m_btnSystemRun.SetSkin(IDB_BITMAP_RUN1, IDB_BITMAP_RUN2, IDB_BITMAP_RUN3, IDB_BITMAP_RUN2, 0, 0, 0, 0, 0);
		m_btnInventoryShow.SetSkin(IDB_BITMAP_SHOW1, IDB_BITMAP_SHOW2, IDB_BITMAP_SHOW3, IDB_BITMAP_SHOW2, 0, 0, 0, 0, 0);
		m_btnInventStat.SetSkin(IDB_BITMAP_STAT1, IDB_BITMAP_STAT2, IDB_BITMAP_STAT3, IDB_BITMAP_STAT2, 0, 0, 0, 0, 0);
		m_btnMaintain.SetSkin(IDB_BITMAP_MAINTAIN1, IDB_BITMAP_MAINTAIN2, IDB_BITMAP_MAINTAIN3, IDB_BITMAP_MAINTAIN2, 0, 0, 0, 0, 0);
		m_btnReport.SetSkin(IDB_BITMAP_REPORT1, IDB_BITMAP_REPORT2, IDB_BITMAP_REPORT3, IDB_BITMAP_REPORT2, 0, 0, 0, 0, 0);
		m_btnAbnormal.SetSkin(IDB_BITMAP_ABNORMAL1, IDB_BITMAP_ABNORMAL2, IDB_BITMAP_ABNORMAL3, IDB_BITMAP_ABNORMAL2, 0, 0, 0, 0, 0);
		m_btnPendant.SetSkin(IDB_BITMAP_PENDANT1, IDB_BITMAP_PENDANT2, IDB_BITMAP_PENDANT3, IDB_BITMAP_PENDANT2, 0, 0, 0, 0, 0);
	}
	
	//创建子对话框
	if (1)
	{
		//获取子对话框位置
		CRect rectDlg;
		GetDlgItem(IDC_STATIC_CHILDDLG)->GetWindowRect(&rectDlg);
		ScreenToClient(&rectDlg);

		//系统运行对话框
		m_cSystemRunDlg.Create(IDD_SYSTEMRUD_DLG, this);
		m_cSystemRunDlg.MoveWindow(rectDlg.left, rectDlg.top, rectDlg.Width(), rectDlg.Height());
		m_cSystemRunDlg.ShowWindow(SW_SHOW);

		//库存展示对话框
		m_cInventoryShowDlg.Create(IDD_INVENTORYSHOW_DLG, this);
		m_cInventoryShowDlg.MoveWindow(rectDlg.left, rectDlg.top, rectDlg.Width(), rectDlg.Height());
		m_cInventoryShowDlg.ShowWindow(SW_HIDE);

		//库存统计对话框
		m_cInventoryStatisticsDlg.Create(IDD__INVENTSTATIC_DLG, this);
		m_cInventoryStatisticsDlg.MoveWindow(rectDlg.left, rectDlg.top, rectDlg.Width(), rectDlg.Height());
		m_cInventoryStatisticsDlg.ShowWindow(SW_HIDE);

		//库位管理对话框
		m_cLocationMaintenanceDlg.Create(IDD_LOCATIONMAINT_DLG, this);
		m_cLocationMaintenanceDlg.MoveWindow(rectDlg.left, rectDlg.top, rectDlg.Width(), rectDlg.Height());
		m_cLocationMaintenanceDlg.ShowWindow(SW_HIDE);

		//库位管理对话框
		m_cReportManageDlg.Create(IDD_REPORTMAINAGE_DLG, this);
		m_cReportManageDlg.MoveWindow(rectDlg.left, rectDlg.top, rectDlg.Width(), rectDlg.Height());
		m_cReportManageDlg.ShowWindow(SW_HIDE);

		//异常信息对话框
		m_cAbnormalStatisticsDlg.Create(IDD_ABNORMAL_DLG, this);
		m_cAbnormalStatisticsDlg.MoveWindow(rectDlg.left, rectDlg.top, rectDlg.Width(), rectDlg.Height());
		m_cAbnormalStatisticsDlg.ShowWindow(SW_HIDE);

		//挂件维护对话框
		m_cPendantMaintDlg.Create(IDD_PENDANTMAINT_DLG, this);
		m_cPendantMaintDlg.MoveWindow(rectDlg.left, rectDlg.top, rectDlg.Width(), rectDlg.Height());
		m_cPendantMaintDlg.ShowWindow(SW_HIDE);
	}

	//获取系统时间并在导航栏显示
	if (1)
	{
		CTime cTime;
		cTime = CTime::GetCurrentTime();

		CString strTime;
		strTime = cTime.Format(_T("%Y-%m-%d"));
		GetDlgItem(IDC_STATIC_TIME)->SetWindowText(strTime);
		UpdateData(false);
	}

	//设置控件字体
	if (1)
	{
		m_cFont.CreatePointFont(120, _T("微软雅黑"));
		GetDlgItem(IDC_STATIC_TIME)->SetFont(&m_cFont);
	}
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CWarehouseMSServerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CWarehouseMSServerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CWarehouseMSServerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



BOOL CWarehouseMSServerDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	
	CRect rcClient;
	GetClientRect(&rcClient);

	BITMAP bm;
	m_BkGndBmp.GetBitmap(&bm);

	CDC memDC;
	memDC.CreateCompatibleDC(pDC);

	CBitmap *pOldBmp = memDC.SelectObject(&m_BkGndBmp);

	pDC->StretchBlt(0, 0, rcClient.Width(), rcClient.Height(), &memDC, 0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);
	memDC.SelectObject(pOldBmp);
	
	memDC.DeleteDC();
	
	return TRUE;

	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CWarehouseMSServerDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO:  在此处添加消息处理程序代码
	//控件随程序自适应全屏后更改位置及大小
	if (1)
	{
		float fsp[2];
		POINT Newp; //获取现在对话框的大小
		CRect recta;
		GetClientRect(&recta);     //取客户区大小  
		Newp.x = recta.right - recta.left;
		Newp.y = recta.bottom - recta.top;
		fsp[0] = (float)Newp.x / m_potOld.x;
		fsp[1] = (float)Newp.y / m_potOld.y;
		CRect Rect;
		int woc;
		CPoint OldTLPoint, TLPoint; //左上角
		CPoint OldBRPoint, BRPoint; //右下角
		HWND  hwndChild = ::GetWindow(m_hWnd, GW_CHILD);  //列出所有控件  
		while (hwndChild)
		{
			woc = ::GetDlgCtrlID(hwndChild);//取得ID
			GetDlgItem(woc)->GetWindowRect(Rect);
			ScreenToClient(Rect);
			OldTLPoint = Rect.TopLeft();
			TLPoint.x = long(OldTLPoint.x*fsp[0]);
			TLPoint.y = long(OldTLPoint.y*fsp[1]);
			OldBRPoint = Rect.BottomRight();
			BRPoint.x = long(OldBRPoint.x *fsp[0]);
			BRPoint.y = long(OldBRPoint.y *fsp[1]);
			Rect.SetRect(TLPoint, BRPoint);
			GetDlgItem(woc)->MoveWindow(Rect, TRUE);
			hwndChild = ::GetWindow(hwndChild, GW_HWNDNEXT);
		}
		m_potOld = Newp;
	}

}

//系统运行按钮响应事件
void CWarehouseMSServerDlg::OnBnClickedBtnRun()
{
	// TODO:  在此添加控件通知处理程序代码
	
	//设置导航菜单按钮激活状态
	m_btnSystemRun.EnableWindow(FALSE);
	m_btnInventoryShow.EnableWindow(TRUE);
	m_btnInventStat.EnableWindow(TRUE);
	m_btnMaintain.EnableWindow(TRUE);
	m_btnReport.EnableWindow(TRUE);
	m_btnAbnormal.EnableWindow(TRUE);
	m_btnPendant.EnableWindow(TRUE);

	//系统运行对话框展示
	m_cSystemRunDlg.ShowWindow(SW_SHOW);

	//其他子对话框隐藏
	m_cInventoryShowDlg.ShowWindow(SW_HIDE);
	m_cInventoryStatisticsDlg.ShowWindow(SW_HIDE);
	m_cLocationMaintenanceDlg.ShowWindow(SW_HIDE);
	m_cReportManageDlg.ShowWindow(SW_HIDE);
	m_cAbnormalStatisticsDlg.ShowWindow(SW_HIDE);
	m_cPendantMaintDlg.ShowWindow(SW_HIDE);
}

//库存展示按钮响应事件
void CWarehouseMSServerDlg::OnBnClickedBtnShow()
{
	// TODO:  在此添加控件通知处理程序代码

	//设置导航菜单按钮激活状态
	m_btnInventoryShow.EnableWindow(FALSE);
	m_btnSystemRun.EnableWindow(TRUE);
	m_btnInventStat.EnableWindow(TRUE);
	m_btnMaintain.EnableWindow(TRUE);
	m_btnReport.EnableWindow(TRUE);
	m_btnAbnormal.EnableWindow(TRUE);
	m_btnPendant.EnableWindow(TRUE);

	//库存展示对话框显示
	m_cInventoryShowDlg.ShowWindow(SW_SHOW);

	//其他子对话框隐藏
	m_cSystemRunDlg.ShowWindow(SW_HIDE);
	m_cInventoryStatisticsDlg.ShowWindow(SW_HIDE);
	m_cLocationMaintenanceDlg.ShowWindow(SW_HIDE);
	m_cReportManageDlg.ShowWindow(SW_HIDE);
	m_cAbnormalStatisticsDlg.ShowWindow(SW_HIDE);
	m_cPendantMaintDlg.ShowWindow(SW_HIDE);

}

//库存统计按钮响应事件
void CWarehouseMSServerDlg::OnBnClickedBtnInventstat()
{
	// TODO:  在此添加控件通知处理程序代码

	//设置导航菜单按钮激活状态
	m_btnInventStat.EnableWindow(FALSE);
	m_btnInventoryShow.EnableWindow(TRUE);
	m_btnSystemRun.EnableWindow(TRUE);
	m_btnMaintain.EnableWindow(TRUE);
	m_btnReport.EnableWindow(TRUE);
	m_btnAbnormal.EnableWindow(TRUE);
	m_btnPendant.EnableWindow(TRUE);

	//库存统计对话框显示
	m_cInventoryStatisticsDlg.ShowWindow(SW_SHOW);

	//其他子对话框隐藏
	m_cSystemRunDlg.ShowWindow(SW_HIDE);
	m_cInventoryShowDlg.ShowWindow(SW_HIDE);
	m_cLocationMaintenanceDlg.ShowWindow(SW_HIDE);
	m_cReportManageDlg.ShowWindow(SW_HIDE);
	m_cAbnormalStatisticsDlg.ShowWindow(SW_HIDE);
	m_cPendantMaintDlg.ShowWindow(SW_HIDE);
}

//库位维护按钮响应事件
void CWarehouseMSServerDlg::OnBnClickedBtnMaintain()
{
	// TODO:  在此添加控件通知处理程序代码

	//设置导航菜单按钮激活状态
	m_btnMaintain.EnableWindow(FALSE);
	m_btnInventStat.EnableWindow(TRUE);
	m_btnInventoryShow.EnableWindow(TRUE);
	m_btnSystemRun.EnableWindow(TRUE);
	m_btnReport.EnableWindow(TRUE);
	m_btnAbnormal.EnableWindow(TRUE);

	//库位维护对话框显示
	m_cLocationMaintenanceDlg.ShowWindow(SW_SHOW);

	//其他子对话框隐藏
	m_cSystemRunDlg.ShowWindow(SW_HIDE);
	m_cInventoryShowDlg.ShowWindow(SW_HIDE);
	m_cInventoryStatisticsDlg.ShowWindow(SW_HIDE);
	m_cReportManageDlg.ShowWindow(SW_HIDE);
	m_cAbnormalStatisticsDlg.ShowWindow(SW_HIDE);
	m_cPendantMaintDlg.ShowWindow(SW_HIDE);
	
}

//报表管理按钮响应事件
void CWarehouseMSServerDlg::OnBnClickedBtnReport()
{
	// TODO:  在此添加控件通知处理程序代码

	//设置导航菜单按钮激活状态
	m_btnReport.EnableWindow(FALSE);
	m_btnMaintain.EnableWindow(TRUE);
	m_btnInventStat.EnableWindow(TRUE);
	m_btnInventoryShow.EnableWindow(TRUE);
	m_btnSystemRun.EnableWindow(TRUE);
	m_btnAbnormal.EnableWindow(TRUE);
	m_btnPendant.EnableWindow(TRUE);
	
	//报表管理对话框显示
	m_cReportManageDlg.ShowWindow(SW_SHOW);

	//其他子对话框隐藏
	m_cSystemRunDlg.ShowWindow(SW_HIDE);
	m_cInventoryShowDlg.ShowWindow(SW_HIDE);
	m_cInventoryStatisticsDlg.ShowWindow(SW_HIDE);
	m_cLocationMaintenanceDlg.ShowWindow(SW_HIDE);
	m_cAbnormalStatisticsDlg.ShowWindow(SW_HIDE);
	m_cPendantMaintDlg.ShowWindow(SW_HIDE);
}

//异常统计按钮响应事件
void CWarehouseMSServerDlg::OnClickedBtnAbnormal()
{
	// TODO:  在此添加控件通知处理程序代码

	//设置导航菜单按钮激活状态
	m_btnAbnormal.EnableWindow(FALSE);
	m_btnReport.EnableWindow(TRUE);
	m_btnMaintain.EnableWindow(TRUE);
	m_btnInventStat.EnableWindow(TRUE);
	m_btnInventoryShow.EnableWindow(TRUE);
	m_btnSystemRun.EnableWindow(TRUE);
	m_btnPendant.EnableWindow(TRUE);
	
	//报表管理对话框显示
	m_cAbnormalStatisticsDlg.ShowWindow(SW_SHOW);

	//其他子对话框隐藏
	m_cSystemRunDlg.ShowWindow(SW_HIDE);
	m_cInventoryShowDlg.ShowWindow(SW_HIDE);
	m_cInventoryStatisticsDlg.ShowWindow(SW_HIDE);
	m_cLocationMaintenanceDlg.ShowWindow(SW_HIDE);
	m_cReportManageDlg.ShowWindow(SW_HIDE);
	m_cPendantMaintDlg.ShowWindow(SW_HIDE);
}

void CWarehouseMSServerDlg::OnBnClickedBtnPendantno()
{
	// TODO:  在此添加控件通知处理程序代码
	//设置导航菜单按钮激活状态
	m_btnPendant.EnableWindow(FALSE);
	m_btnReport.EnableWindow(TRUE);
	m_btnMaintain.EnableWindow(TRUE);
	m_btnInventStat.EnableWindow(TRUE);
	m_btnInventoryShow.EnableWindow(TRUE);
	m_btnSystemRun.EnableWindow(TRUE);
	m_btnAbnormal.EnableWindow(TRUE);

	//报表管理对话框显示
	m_cPendantMaintDlg.ShowWindow(SW_SHOW);

	//其他子对话框隐藏
	m_cSystemRunDlg.ShowWindow(SW_HIDE);
	m_cInventoryShowDlg.ShowWindow(SW_HIDE);
	m_cInventoryStatisticsDlg.ShowWindow(SW_HIDE);
	m_cLocationMaintenanceDlg.ShowWindow(SW_HIDE);
	m_cReportManageDlg.ShowWindow(SW_HIDE);
	m_cAbnormalStatisticsDlg.ShowWindow(SW_HIDE);
}



HBRUSH CWarehouseMSServerDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  在此更改 DC 的任何特性
	if (pWnd->GetDlgCtrlID() == (IDC_STATIC_TIME))
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(255, 255, 255));
		return HBRUSH(GetStockObject(HOLLOW_BRUSH));
	}

	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
	return hbr;
}


//获取数据库连接串
void CWarehouseMSServerDlg::GetDBInfo()
{
	//获取执行文件路径
	HMODULE module = GetModuleHandle(0);
	TCHAR chFileName[MAX_PATH];
	GetModuleFileName(module, chFileName, MAX_PATH);

	CString strFullPath;
	strFullPath.Format(_T("%s"), chFileName);

	//删除执行文件名
	int nPot = strFullPath.ReverseFind('\\');
	strFullPath = strFullPath.Left(nPot);

	//将配置文件名拼接到打开文件路径中
	CString strFileName = _T("sqlconn.ini");
	strFullPath += _T("\\") + strFileName;

	//定义文件变量
	CStdioFile file(strFullPath, CStdioFile::modeRead);

	CString strConn;
	//读取SQLServer数据库配置信息
	file.ReadString(strConn);

	_strDBInfo = strConn;

}

