
// WarehouseMSServerDlg.h : 头文件
//

#pragma once
#include "xSkinButton.h"
#include "SystemRunDlg.h"
#include "InventoryShowDlg.h"
#include "InventoryStatisticsDlg.h"
#include "LocationMaintenanceDlg.h"
#include "ReportManageDlg.h"
#include "AbnormalStatisticsDlg.h"
#include "PendantMaintDlg.h"

// CWarehouseMSServerDlg 对话框
class CWarehouseMSServerDlg : public CDialogEx
{
// 构造
public:
	CWarehouseMSServerDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_WAREHOUSEMSSERVER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

//自定义
public:
	CBitmap m_BkGndBmp;
	//原始屏幕坐标
	POINT m_potOld;
	//控件字体
	CFont m_cFont;

	//定义按钮对象
	CxSkinButton m_btnOK;//退出按钮
	CxSkinButton m_btnSystemRun;//系统运行按钮
	CxSkinButton m_btnInventoryShow;//库位展示按钮
	CxSkinButton m_btnInventStat;//库存统计按钮
	CxSkinButton m_btnMaintain;//库位维护按钮
	CxSkinButton m_btnReport;//库位维护按钮
	CxSkinButton m_btnAbnormal;//库位维护按钮
	CxSkinButton m_btnPendant;//库位维护按钮

	//子对话框定义区
	CSystemRunDlg m_cSystemRunDlg;//系统运行对话框
	CInventoryShowDlg m_cInventoryShowDlg;//库存展示对话框
	CInventoryStatisticsDlg m_cInventoryStatisticsDlg;//库存统计对话框
	CLocationMaintenanceDlg m_cLocationMaintenanceDlg;//库位维护对话框
	CReportManageDlg m_cReportManageDlg;//报表管理对话框
	CAbnormalStatisticsDlg m_cAbnormalStatisticsDlg;//异常统计对话框
	CPendantMaintDlg m_cPendantMaintDlg;//挂件维护对话框

	//程序启动获取数据库连接串
	void GetDBInfo();

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedBtnRun();
	afx_msg void OnBnClickedBtnShow();
	afx_msg void OnBnClickedBtnInventstat();
	afx_msg void OnBnClickedBtnMaintain();
	afx_msg void OnBnClickedBtnReport();
	afx_msg void OnClickedBtnAbnormal();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnPendantno();
};
